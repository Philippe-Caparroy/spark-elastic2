/**
 * 
 */
package fr.dga.soc.spark;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.net.URI;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.spark.launcher.SparkLauncher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import fr.dga.soc.elasticsearch.ElasticsearchManager;
import fr.dga.soc.elasticsearch.kibana.KibanaClient;
import fr.dga.soc.feeds.FeedsManager;
import fr.dga.soc.feeds.misp.MispClient;
import fr.dga.soc.utils.ElasticsearchConfig;
import fr.dga.soc.utils.IocConfig;
import fr.dga.soc.utils.LogConfig;
import fr.dga.soc.utils.MispConfig;
import fr.dga.soc.utils.SparkClientConfig;
import fr.dga.soc.utils.SparkConfig;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/**
 * @author pcaparroy
 *
 */
@Command(name = "sparkcli", mixinStandardHelpOptions = true, version = "${cproject.version}", description = "The command-line Spark client.")
public class SparkClient implements Runnable, Serializable {

	/** The logger. */
	static Logger logger = LoggerFactory.getLogger(SparkClient.class);


	/** The command. */
	@Parameters(index = "0", description = "One of bootstrap: bootstrap the elasticsearch cluster for supervision , bloomFilter: generate sha256 bloom filter, detect: load hashes from log index and identify iocs positives.")
	private String command;

	/** The from date. */
	@Option(names = { "-fr",
			"--from" }, description = "A start date formatted in iso-date format (yyyy-MM-dd) that will be apply to filter iocs files including a date greater or equal.")
	String fromDate;

	@Option(names = { "-t",
	"--type" }, description = "The type of ioc concerned : ip-dst,url,filename,domain,sha1,md5,sha256,ip-dst-port.")
	String iocType;
	
	@Option(names = { "-x",
	"--test" }, description = "A flag for test purpose.")
	boolean isTest;
	
	/**
	 * 
	 */
	public SparkClient() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Inits the logging.
	 *
	 * @param command       the command
	 * @param isReinjection the is reinjection
	 */
	private static void initLogging(String command) {

		FileAppender fa = new FileAppender();
		fa.setName("FileLogger");

		String loggingPath = "/var/log/elastioc/";
		File file = new File(loggingPath);
		if (!file.exists()) {
			try {
				file.mkdir();
				Path path = Paths.get("/var/log/elastioc/");

				//java.nio.file.Files;
				Files.createDirectories(path);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		fa.setFile(loggingPath + getLoggingFileName(command));
		fa.setLayout(new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %-5p [%c{1}] %m%n"));
		fa.setThreshold(Level.INFO);
		fa.setAppend(true);
		fa.activateOptions();

		// add appender to any Logger (here is root)
		org.apache.log4j.Logger.getRootLogger().addAppender(fa);

		ConsoleAppender console = new ConsoleAppender(new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %-5p [%c{1}] %m%n"),
				"System.out");
		console.setName("console");
		console.setThreshold(Level.INFO);

		console.activateOptions();
		org.apache.log4j.Logger.getRootLogger().addAppender(console);

		org.apache.log4j.Logger.getLogger("org.apache.hadoop").setLevel(Level.ERROR);
		org.apache.log4j.Logger.getLogger("org.apache.spark").setLevel(Level.ERROR);
		org.apache.log4j.Logger.getLogger("org.apache.kafka").setLevel(Level.ERROR);

		org.apache.log4j.Logger.getLogger("org.apache.flink").setLevel(Level.INFO);
		// to suppress the outputs of SparkLauncher

	}

	/**
	 * Gets the logging file name.
	 *
	 * @param command       the command
	 * @param isReinjection the is reinjection
	 * @return the logging file name
	 */
	private static String getLoggingFileName(String command) {

		String timeStr = LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.of("UTC"))
				.format(DateTimeFormatter.ISO_DATE_TIME);
		String fileName = "/elastioc-" + command + "-" + timeStr + ".log";


		return fileName;
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int exitCode = new CommandLine(new SparkClient()).execute(args);
		System.exit(exitCode);

	}

	@Override
	public void run() {
		
		initLogging(command);
		
		Optional<SparkClientConfig> config = loadConfig();

		if(config.isPresent()) {

		
		switch (command) {
		
			case "bootstrap":
				bootstrapElasticsearch(config.get());
				break;
			case "download":
				downloadFeeds(config.get());
				break;
				
			case "deduplicate":
				deduplicateVtJob(config.get());
				break;
	
			case "index":
				indexFeeds(config.get());
				//indexVtJob(config.get());
				break;
	
			case "generateFilters":
				generateFilters(config.get());
				break;
				
			case "detect":
				
				detectAllIocs(config.get());
	
				break;
			case "test":
				isTest=true;
				generateTests(config.get());
			//	generateFilters(config.get(), true);
				//detectAllIocs(config.get(),true);
				break;

			case "ingest":
				
				logger.info("running full ingestion cycle");
				ingest(config.get());
	
				break;

			default:
				
				break;
			}
		
		}
	}
	
	private void generateTests(SparkClientConfig config) {
		try {
			
			FeedsManager feedsManager = new FeedsManager(config);
			
			feedsManager.generateIocTests(config,iocType);
		
		} catch (Exception e) {
			
			logger.error("Failed to generate iocs tests",e);
		}
	}
	
	private void ingest(SparkClientConfig aConfig) {
		
		bootstrapElasticsearch(aConfig);
		
		Optional<Set<String>> newIocTypes = downloadFeeds(aConfig);

		processIocs(aConfig, newIocTypes,false);
	}
	
	
	private void processIocs(SparkClientConfig config,Optional<Set<String>> newIocTypes,boolean isTest) {
		
		Map<String,IocConfig> iocsConfigs = config.getIocs();
		
		for(Entry<String, IocConfig> entry: iocsConfigs.entrySet()) {
			
			String iocId = entry.getKey();
			
			IocConfig iocConfig = entry.getValue();
			
			if(!iocConfig.isDeactivated()) {
			
				if(filterGenerationIsNecessary(iocId, newIocTypes, iocConfig)) {
					
					generateDetectionFilter(iocId, iocConfig, config.getSpark(), config.getElasticsearch(),false);
				}
				
				detectIocs(iocId, iocConfig, config.getSpark(), config.getElasticsearch(),isTest);
			}
		}
		
	}
	
	private boolean filterGenerationIsNecessary(String iocId,Optional<Set<String>> newIocTypes,IocConfig iocConfig) {
		boolean necessary=false;
		String iocType=iocId.substring(iocId.indexOf("-")+1);
		if(newIocTypes.isPresent()) {
			
			if(newIocTypes.get().contains(iocType)) {
		
				necessary=true;
			}
		}else {
			
			String bloomFilterPath = iocConfig.getBloomFilterPath();
			File file = new File(bloomFilterPath);
			if(!file.exists()) {
				necessary = true;
			}
		}
		
		return necessary;
	}
	

	
	
	private Optional<Set<String>> indexFeeds(SparkClientConfig aConfig) {
		return updateFeeds(aConfig, FeedsManager.INDEX_IOC_ACTION);
	}
	
	private Optional<Set<String>> downloadFeeds(SparkClientConfig aConfig) {
		return updateFeeds(aConfig, FeedsManager.DOWNLOAD_ACTION);
	}
	
	private Optional<Set<String>> updateFeeds(SparkClientConfig aConfig, String updateAction) {
		Optional<Set<String>> newIocTypes = Optional.empty();
		
		try {
			
			FeedsManager feedsManager = new FeedsManager(aConfig);
			
			Optional<String> startDateOption = Optional.ofNullable(fromDate);
			
			LocalDate startDate = LocalDate.now();
			if(startDateOption.isPresent()) {
				try {
					startDate = LocalDate.parse(startDateOption.get());
				} catch (DateTimeParseException e) {
					logger.error("Incorrect from option : {}",fromDate);
				}
				
			}
			
			Set<String> newIocTypesSet = feedsManager.updateAllFeeds(startDate,updateAction);
			
			if(newIocTypesSet.size()>0) {
				newIocTypes = Optional.of(newIocTypesSet);
			}
		
		} catch (Exception e) {
			
			logger.error("Failed to update iocs feeds",e);
		}
		
		return newIocTypes;
	}
	
	private void detectAllIocs(SparkClientConfig config) {
		
		Map<String,IocConfig> iocsConfigs = config.getIocs();
		
		for(Entry<String, IocConfig> entry: iocsConfigs.entrySet()) {
			
			String iocId = entry.getKey();
			
			IocConfig iocConfig = entry.getValue();
			
			if(!iocConfig.isDeactivated()) {
				
				if(iocType!=null) {
					
					String currentType = iocId.substring(iocId.indexOf("-")+1);
					
					if(iocType.equals(currentType)) {

						detectIocs(iocId, iocConfig, config.getSpark(), config.getElasticsearch(),isTest);

					}
					
				}else {
				
					detectIocs(iocId, iocConfig, config.getSpark(), config.getElasticsearch(),isTest);
				
				}
			}
		}
		
	}
	
	private void detectIocs(String iocId,IocConfig iocConfig,SparkConfig sparkConfig,ElasticsearchConfig elasticsearchConfig,boolean isTest) {
		
		if(iocConfig.isDeactivated()) {
			return;
		}
		
		Map<String,LogConfig> targetLogsConfigs = iocConfig.getTargets();
		
		if(targetLogsConfigs!=null) {
			
			for(Entry<String,LogConfig> targetEntry:targetLogsConfigs.entrySet()) {
				
				String targetId = targetEntry.getKey();
				
				LogConfig logConfig = targetEntry.getValue();
				
				detectIocsJob(sparkConfig,
								elasticsearchConfig,
								iocConfig.getIndexName(),
								iocConfig.getTruePositivesDetectionWindow(),
								iocConfig.getBloomFilterPath(),
								iocId,
								logConfig,isTest);
				
				if(isTest) {
					break;
				}
					
			}
			
		}
		
	}
	
	private void bootstrapElasticsearch(SparkClientConfig config) {
		try {
						
			importKibanaSavedObject(config.getElasticsearch());
			
			importTemplates(config);
			
		} catch (Exception e) {
			logger.error("Failed to bootstrap Elasticsearch cluster for supervision",e);
		}
		
	}
	
	private void importKibanaSavedObject(ElasticsearchConfig aConfig) {
		
		try {
			
			KibanaClient kibanaClient = new KibanaClient(aConfig);
			
			kibanaClient.initialize();
			
			Path savedObjectsDirectory = Paths.get(new URI("file://"+aConfig.getKibanaSavedObjectsPath()));
			
			List<Path> savedObjectsPaths = Files.walk(savedObjectsDirectory,1,FileVisitOption.FOLLOW_LINKS)
													.filter(path->path.toAbsolutePath().toString().endsWith("ndjson"))
													.collect(Collectors.toList());
			
			savedObjectsPaths.remove(savedObjectsDirectory.toAbsolutePath());
			
			
				for(Path savedObjectPath: savedObjectsPaths) {
					
					String saveObjectFileName = savedObjectPath.getFileName().toString();
					
					kibanaClient.importSavedObject(savedObjectPath.toAbsolutePath().toString(),saveObjectFileName);
				
				}
			
				
		} catch (Exception e) {
			logger.error("Failed to import Kibana saved objects",e);
		}
		
	}
	
	private void importTemplates(SparkClientConfig aConfig) {
		try {
		
			ElasticsearchManager elastic = new ElasticsearchManager(aConfig.getElasticsearch());
			
			elastic.updateComponentTemplates();
			
			elastic.updateIlms();
			
			elastic.updateTemplates();
			
		} catch (Exception e) {
			logger.error("Failed to import/update component templates",e);
		}
		
		
	}
	private SparkLauncher getSparkLauncher(String mainClass,String appName,SparkConfig spark) {
		Map<String, String> env = new HashMap<String, String>();

		SparkLauncher sparkProcess = new SparkLauncher(env)
				.setVerbose(true)
				.setAppResource("/opt/spark/jars/elastioc.jar")
				.setMainClass(mainClass)
				.setDeployMode("client")
				.setMaster(spark.getMasterUrl())
				.setAppName(appName)
				.setSparkHome("/opt/spark")
				.setConf("spark.executor.memory",spark.getExecutorMemory())
				.setConf("spark.driver.memory",spark.getDriverMemory());
		if(spark.getJava_home()!=null) {
			sparkProcess.setJavaHome(spark.getJava_home());
		}
		return sparkProcess;
	}

	private void deduplicateVtJob(SparkClientConfig config) {

		if(config.getIocs().containsKey("virusTotal-files")) {
		
			IocConfig vtConfig = config.getIocs().get("virusTotal-files");
		
			logger.info("deduplicating Virus total sha256.");
			
		    String inputDir = vtConfig.getRawDataPath();
		    
		    String outputDir = vtConfig.getDeduplicatedDataPath();
		    
			if(inputDir!=null && outputDir!=null) {
				
				SparkLauncher sparkProcess = getSparkLauncher("fr.dga.soc.spark.jobs.DeduplicatingVirusTotalJob","deduplicate VT",config.getSpark());
	
				sparkProcess.addAppArgs("--inputDir",inputDir,"--outputDir",outputDir);
	
				SparkMonitor monitor = new SparkMonitor();
	
				monitor.monitorJob(sparkProcess, "VirusTotal dedup");
			}else {
				logger.error("You must provite valid input and output directories!");
			}
		}else {
			logger.info("config has no virus-total ioc parameters");
		}

	}

	private void indexVtJob(SparkClientConfig config) {

		if(config.getIocs().containsKey("virusTotal-files")) {
			
			IocConfig vtConfig = config.getIocs().get("virusTotal-files");
			
			String inputDir = vtConfig.getDeduplicatedDataPath();
		try {

			if(inputDir!=null) {

				logger.info("Indexing Virus total sha256's.");

				SparkLauncher sparkProcess = getSparkLauncher("fr.dga.soc.spark.jobs.VirusTotalIndexingJob","VtIndexing",config.getSpark());

				sparkProcess.addAppArgs("--inputDir",inputDir,"--esNodes",config.getElasticsearch().getNodes(),"--vtIndexName",vtConfig.getIndexName());

				SparkMonitor monitor = new SparkMonitor();

				monitor.monitorJob(sparkProcess, "VirusTotal indexing");
			}else {
				logger.error("You must provide a valid input directory");
			}

		} catch (Exception e) {

			logger.error("Failed to index Virus total",e);

		}

		}else {
			logger.info("config has no virus-total ioc parameters");
		}
	}

	private void generateFilters(SparkClientConfig config) {

		for(Entry<String,IocConfig> entry:config.getIocs().entrySet()) {
		
			String iocId = entry.getKey();
			
			IocConfig iocConfig = entry.getValue();
			
			if(iocType!=null) {
				
				String currentType = iocId.substring(iocId.indexOf("-")+1);
				if(currentType.equals(iocType)) {
					generateDetectionFilter(iocId, iocConfig, config.getSpark(), config.getElasticsearch(),isTest);

				}
		
			}else {
				generateDetectionFilter(iocId, iocConfig, config.getSpark(), config.getElasticsearch(),isTest);

			}
		}

	}
	
	
	private void generateDetectionFilter(String iocId,IocConfig iocConfig,SparkConfig sparkConfig,ElasticsearchConfig elasticsearchConfig,boolean isTest) {
		
		if(iocConfig.isDeactivated()) {
			return;
		}
		
		String encoding="elasticsearch";
		
		if(iocId.equals("virusTotal-files")) {
			encoding="parquet";
		}
		
		
		String bloomInputDir = iocConfig.getDeduplicatedDataPath();
		if(bloomInputDir==null) {
			bloomInputDir=iocConfig.getRawDataPath();
		}
		
		String bloomOutput = iocConfig.getBloomFilterPath();
		if(isTest) {
			bloomOutput=bloomOutput+"_test";
		}
		try {

			if(bloomOutput!=null) {

				logger.info("Building "+iocId+" bloom filter.");

				SparkLauncher sparkProcess = getSparkLauncher("fr.dga.soc.spark.jobs.BloomFilterBuildJob","Build "+iocId+" BloomFilter",sparkConfig);

				sparkProcess.addAppArgs("--bloomOutputPath",bloomOutput,"--iocId",iocId,"--encoding",encoding);
				if(bloomInputDir!=null) {
					sparkProcess.addAppArgs("--bloomInputPath",bloomInputDir);
				}
				
				String iocsIndexName = iocConfig.getIndexName();
				if(isTest) {
					iocsIndexName=iocsIndexName+"-"+iocId.substring(iocId.indexOf("-")+1)+"-test";
				}
				
				sparkProcess.addAppArgs("--indexName",iocsIndexName,
				"--sparkMaster",sparkConfig.getMasterUrl(),
				"--driverMemory",sparkConfig.getDriverMemory(),
				"--executorMemory",sparkConfig.getExecutorMemory());
				
				sparkProcess.addAppArgs("--esNodes",elasticsearchConfig.getNodes());
					
				
				if(iocConfig.getHashSourceFieldName()!=null) {
					sparkProcess.addAppArgs("--hashSourceFieldName",iocConfig.getHashSourceFieldName());
				}
				
				

				SparkMonitor monitor = new SparkMonitor();

				monitor.monitorJob(sparkProcess, iocId+" Bloom filter generation");
			}
			
		} catch (Exception e) {

			logger.error("Failed to build  bloom filter for ioc {}",iocId,e);

		}
		
	}

	

	private void detectIocsJob(SparkConfig spark,
								ElasticsearchConfig esConfig,
								String iocIndexName,
								String truePositivesDetectionWindow,
								String bloomFilterPath,
								String iocId,
								LogConfig targetLogConfig,
								boolean isTest) {

		try {


			SparkLauncher sparkProcess = getSparkLauncher("fr.dga.soc.spark.jobs.DetectIocsJob",iocId+"-IocDetection",spark);

			String sha256FieldAlias = targetLogConfig.getSha256FieldAlias();

			String sourceIndexPattern = targetLogConfig.getIndexPattern();
			
			if(isTest) {
				sourceIndexPattern=sourceIndexPattern.replaceAll("\\*", iocId.substring(iocId.indexOf("-")+1)+"-test");
				bloomFilterPath = bloomFilterPath+"_test";
				iocIndexName = iocIndexName+"-"+iocId.substring(iocId.indexOf("-")+1)+"-test";
			}

			logger.info("detecting {} hashes from {} logs.",iocId,sourceIndexPattern);

			sparkProcess.addAppArgs("--iocId",iocId,
									"--sha256FieldAlias",sha256FieldAlias,
									"--indexPattern",sourceIndexPattern,
									"--esNodes",esConfig.getNodes(),
									"--bloomInput",bloomFilterPath,
									"--iocIndexName",iocIndexName,
									"--truePositivesIndexName",(iocIndexName+"-true-positives"),
									"--truePositivesDetectionWindow",truePositivesDetectionWindow,
									"--sparkMaster",spark.getMasterUrl(),
									"--driverMemory",spark.getDriverMemory(),
									"--executorMemory",spark.getExecutorMemory());

			SparkMonitor monitor = new SparkMonitor();

			monitor.monitorJob(sparkProcess, iocId+" positives detection "+sourceIndexPattern);

		} catch (Exception e) {

			logger.error("Failed to monitor ioc {}",iocId,e);

		}


	}

	public static Optional<SparkClientConfig> loadConfig() {
		
		Optional<SparkClientConfig> config = Optional.empty();
		try {
			Path configPath = Paths.get("/etc/elastioc/config.yml");
			Constructor constructor = new Constructor(SparkClientConfig.class);
			Yaml yaml = new Yaml(constructor);
			config = Optional.of(yaml.load(new FileInputStream(configPath.toFile())));
		} catch (Exception e) {
			logger.error("Failed to load yaml config",e);
		}
		
		return config;
	}





}
