#!/bin/bash
set -eu
# setup log dir
export LOG_DIR=/var/log/elastioc
mkdir -p $LOG_DIR
#chown -R hdfs:hadoop $LOG_DIR

if [[ $# -le 0 ]]; then
    echo 'At least a role or a command is required!' >&2 && exit 1
fi

role=$1

case $role in
    'elastioc')
		service cron start
		crontab -u root /etc/cron.d/elastioc-cron
		elastioc
        ;;
         
esac


exec /bin/bash