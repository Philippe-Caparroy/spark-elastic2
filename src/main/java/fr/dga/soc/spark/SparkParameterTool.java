/**
 * 
 */
package fr.dga.soc.spark;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import org.apache.commons.lang3.math.NumberUtils;

// TODO: enrich progressively Javadoc
/**
 * The Class SparkParameterTool.
 *
 * @author pcaparroy
 */
public class SparkParameterTool {

	/** The no value key. */
	public static String NO_VALUE_KEY="NO_VALUE_KEY";
	/**
	 * Returns {@link SparkParameterTool} for the given arguments. The arguments are keys followed by values.
	 * Keys have to start with '-' or '--'
	 *
	 * <p><strong>Example arguments:</strong>
	 * --key1 value1 --key2 value2 -key3 value3
	 *
	 * @param args Input array arguments
	 * @return A {@link SparkParameterTool}
	 */
	public static SparkParameterTool fromArgs(String[] args) {
		
		final Map<String, String> map = new HashMap(args.length / 2);

		int i = 0;
		while (i < args.length) {
			final String key = getKeyFromArgs(args, i);

			if (key.isEmpty()) {
				throw new IllegalArgumentException(
					"The input " + Arrays.toString(args) + " contains an empty argument");
			}

			i += 1; // try to find the value

			if (i >= args.length) {
				map.put(key, NO_VALUE_KEY);
			} else if (NumberUtils.isNumber(args[i])) {
				map.put(key, args[i]);
				i += 1;
			} else if (args[i].startsWith("--") || args[i].startsWith("-")) {
				// the argument cannot be a negative number because we checked earlier
				// -> the next argument is a parameter name
				map.put(key, NO_VALUE_KEY);
			} else {
				map.put(key, args[i]);
				i += 1;
			}
		}

		return fromMap(map);
	}
	
	/** The data. */
	protected final Map<String, String> data;

	/**
	 * Instantiates a new spark parameter tool.
	 *
	 * @param data the data
	 */
	private SparkParameterTool(Map<String, String> data) {
		this.data = Collections.unmodifiableMap(new HashMap(data));
	}	
	/**
	 * Get the key from the given args. Keys have to start with '-' or '--'. For example, --key1 value1 -key2 value2.
	 * @param args all given args.
	 * @param index the index of args to be parsed.
	 * @return the key of the given arg.
	 */
	public static String getKeyFromArgs(String[] args, int index) {
		String key;
		if (args[index].startsWith("--")) {
			key = args[index].substring(2);
		} else if (args[index].startsWith("-")) {
			key = args[index].substring(1);
		} else {
			throw new IllegalArgumentException(
				String.format("Error parsing arguments '%s' on '%s'. Please prefix keys with -- or -.",
					Arrays.toString(args), args[index]));
		}

		if (key.isEmpty()) {
			throw new IllegalArgumentException(
				"The input " + Arrays.toString(args) + " contains an empty argument");
		}

		return key;
	}
	// ------------------ Get data from the util ----------------

		/**
	 * Returns number of parameters in {@link SparkParameterTool}.
	 *
	 * @return the number of parameters
	 */
		
		public int getNumberOfParameters() {
			return data.size();
		}

		/**
		 * Returns the String value for the given key.
		 * If the key does not exist it will return null.
		 *
		 * @param key the key
		 * @return the string
		 */
		
		public String get(String key) {
			
			return data.get(key);
		}

		/**
		 * Check if value is set.
		 *
		 * @param value the value
		 * @return true, if successful
		 */
		
		public boolean has(String value) {
			
			return data.containsKey(value);
		}
		

		
		/**
		 * To map.
		 *
		 * @return the map
		 */
		public Map<String, String> toMap() {
			return data;
		}
	/**
	 * Returns {@link SparkParameterTool} for the given map.
	 *
	 * @param map A map of arguments. Both Key and Value have to be Strings
	 * @return A {@link SparkParameterTool}
	 */
	public static SparkParameterTool fromMap(Map<String, String> map) {
		checkNotNull(map, "Unable to initialize from empty map");
		return new SparkParameterTool(map);
	}
	
	/**
	 * Check not null.
	 *
	 * @param <T> the generic type
	 * @param reference the reference
	 * @param errorMessage the error message
	 * @return the t
	 */
	public static <T> T checkNotNull(T reference, @Nullable String errorMessage) {
		if (reference == null) {
			throw new NullPointerException(String.valueOf(errorMessage));
		}
		return reference;
	}

}
