/**
 * 
 */
package fr.dga.soc.spark.jobs;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.concat_ws;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StringType;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.util.LongAccumulator;
import org.apache.spark.util.sketch.BloomFilter;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.dga.soc.VtJob;
import fr.dga.soc.spark.SparkParameterTool;
import fr.dga.soc.spark.functions.DeduplicateFunction;
import fr.dga.soc.spark.functions.Doc2IocFunction;
import fr.dga.soc.spark.functions.GenericIndexingFunction;
import fr.dga.soc.spark.functions.HashValue;
import fr.dga.soc.spark.functions.Sha256;
import fr.dga.soc.spark.functions.Sha256Function;
import fr.dga.soc.utils.IocUtils;
import scala.Tuple2;

/**
 * @author pcaparroy
 *
 */
public class BloomFilterBuildJob {

	private static Logger logger = LoggerFactory.getLogger(BloomFilterBuildJob.class);

	/**
	 * 
	 */
	public BloomFilterBuildJob() {
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		SparkParameterTool parameters = SparkParameterTool.fromArgs(args);
		
		String iocId = parameters.get("iocId");
		 
		logger.info("generating bloom-filter for {}",iocId);
	
		String hashSourceFieldName = parameters.get("hashSourceFieldName");
		if(hashSourceFieldName==null) {
			hashSourceFieldName = iocId.substring(iocId.indexOf("-")+1);
		}
		
		if(hashSourceFieldName.equals("ip-dst-port")) {
			hashSourceFieldName="ip-dst|port";
		}
		
		boolean generateSha256 = false;
		if(IocUtils.needsSha256Conversion(hashSourceFieldName)) {
			generateSha256=true;
		}
		
		String hashFieldName = generateSha256?"sha256":hashSourceFieldName;
		
		String esNodes = parameters.get("esNodes");
		String encoding = parameters.get("encoding");
		
		
		
		String inputPath = parameters.get("bloomInputPath");
		String bloomFilterOutputPath = parameters.get("bloomOutputPath");
		
		String iocIndexName=parameters.get("indexName");
		

		String sparkMasterUrl =  parameters.get("sparkMaster");
		
		String driverMemory = parameters.get("driverMemory");
		
		String executorMemory = parameters.get("executorMemory");
		
		long start = System.currentTimeMillis();
		
		SparkConf conf = new SparkConf()
						.setAppName(iocId+" hash Bloomfilter generation")
						.setMaster(sparkMasterUrl)
						.set("es.nodes",esNodes)
						.set("es.input.max.docs.per.partition","10000");
		
		if(executorMemory!=null) {
			conf.set("spark.executor.memory",executorMemory);
		}
		
		if(driverMemory!=null) {
			conf.set("spark.driver.memory",driverMemory);
		}
		
		if(encoding.equals("elasticsearch")) {
			String query = "{ \"query\":{\n"
					+ "    \"exists\": {\n"
					+ "      \"field\": \"IOC_FIELD\"\n"
					+ "    }}}\n"
					;
			
			query = query.replaceAll("IOC_FIELD", hashSourceFieldName);
			conf.set("es.query",query);
			
		}
		
		JavaSparkContext jsc = new JavaSparkContext(conf);
		
		SparkSession spark = SparkSession.builder().sparkContext(jsc.sc()).getOrCreate();
		
		try {

			Optional<Dataset<Row>> rawDataset = readRawData(spark, inputPath, encoding,hashSourceFieldName,jsc,iocIndexName);

			if(rawDataset.isPresent()) {
				
				long numItems = rawDataset.get().count();
				
				logger.info("{} {} collected",numItems,hashSourceFieldName);
				if(numItems>0) {
					
				//rawDataset.get().show(100, false);
				String[] fieldNames = rawDataset.get().schema().fieldNames();
				
				if(iocIndexName.equals("iocs-virus-total")) {
				
					JavaRDD<Map<String,Object>> indexableRdd = rawDataset.get().javaRDD().map(new GenericIndexingFunction(hashFieldName,hashSourceFieldName,fieldNames,generateSha256));
					
					JavaEsSpark.saveToEs(indexableRdd, iocIndexName);
				
				}
				
				BloomFilter bf = rawDataset.get().stat().bloomFilter(col(hashSourceFieldName), numItems, 0.005);
	
				bf.writeTo(new FileOutputStream(new File(bloomFilterOutputPath)));
				} else {
					logger.info("no {} iocs in elasticsearch index {}: skipping bloom filter generation",hashSourceFieldName,iocIndexName);
				}
				
				double duration = (System.currentTimeMillis() - start) / 1000;
				
				System.out.println("duration(s): " + duration);
				
				logger.info("Bloom filter with "+numItems + " "+iocId+" hashes generated in "+duration+" ms.");
			}

		} catch (Exception e) {

			logger.error("Failed to generate bloom filter",e);

		} finally {
			
			spark.close();
		
		}


	}
	
	private static Optional<Dataset<Row>> readRawData(SparkSession spark,String inputPath,String encoding,String hashSourceFieldName,JavaSparkContext jsc,String indexName){
		
		Optional<Dataset<Row>> rawDataset = Optional.empty();
		
		switch (encoding) {
		
			case "parquet":
				
				Dataset<Row> rawParquet = spark.read()
												.parquet(inputPath + "/*")
												.filter(col("av_positives").gt(4));
				
				StructType parquetSchema = rawParquet.schema();
				
				LongAccumulator duplicatesCounter = spark.sparkContext().longAccumulator();
				
				JavaRDD<Row> deduplicatedParquet =rawParquet.javaRDD().keyBy(row->(String)row.getAs("sha256")).groupByKey().map(new DeduplicateFunction(duplicatesCounter));
				
				rawDataset = Optional.of(spark.sqlContext().createDataFrame(deduplicatedParquet,parquetSchema));
				
				break;
				
			case "elasticsearch":
				
				JavaPairRDD<String,Map<String,Object>> iocsRdd = JavaEsSpark.esRDD(jsc, indexName);
				
				boolean generateSha256 = IocUtils.needsSha256Conversion(hashSourceFieldName);
				
				JavaRDD<Map<String,String>> iocRdd = iocsRdd.values().mapPartitions(new Doc2IocFunction(hashSourceFieldName,generateSha256));
				
				JavaRDD<Row> rows = iocRdd.map(new Function<Map<String,String>, Row>() {
													    @Override
													    public Row call(Map<String,String> record) throws Exception {
													        return RowFactory.create(record.get(hashSourceFieldName));
													    }
													});
			
				List<StructField> fields = new ArrayList();
				StructField field = DataTypes.createStructField(hashSourceFieldName, DataTypes.StringType, true);
				fields.add(field);
				
				StructType schema = DataTypes.createStructType(fields);
				rawDataset = Optional.of(spark.createDataFrame(rows,schema).distinct());
				break;
				
			case "csv":
				
				StructType csvSchema = DataTypes.createStructType(new StructField[]{DataTypes.createStructField(hashSourceFieldName, DataTypes.StringType, true)});
				
				rawDataset = Optional.of(spark.read().option("header","false").option("timestampFormat", "yyyy/MM/dd HH:mm:ss ZZ").schema(csvSchema).csv(inputPath));
				
				break;
				
			default:
				
				logger.warn("Unknown encoding provided {}",encoding);
				
				break;
		}
		return rawDataset;
	}

	
}
