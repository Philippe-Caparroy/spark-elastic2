/**
 * 
 */
package fr.dga.soc.spark.jobs;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.util.LongAccumulator;
import org.apache.spark.util.sketch.BloomFilter;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.hash.Hashing;

import fr.dga.soc.VtJob.BloomFilterFunction2;
import fr.dga.soc.elasticsearch.ElasticsearchManager;
import fr.dga.soc.feeds.misp.MispClient;
import fr.dga.soc.spark.SparkClient;
import fr.dga.soc.spark.SparkParameterTool;
import fr.dga.soc.spark.functions.BloomFilterFunction;
import fr.dga.soc.spark.functions.CheckIocPositivesFunction;
import fr.dga.soc.utils.IocUtils;
import fr.dga.soc.utils.MispConfig;
import fr.dga.soc.utils.SparkClientConfig;
import scala.Tuple2;

/**
 * @author pcaparroy
 *
 */
public class DetectIocsJob {

	private static Logger logger = LoggerFactory.getLogger(DetectIocsJob.class);

	/**
	 * 
	 */
	public DetectIocsJob() {
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SparkParameterTool parameters = SparkParameterTool.fromArgs(args);

		String iocId = parameters.get("iocId");
		
		String isTest = parameters.get("isTest");
		
		String sha256FieldAlias = parameters.get("sha256FieldAlias");
		
		String hashSourceFieldName = iocId.substring(iocId.indexOf("-")+1);

		String[] destinationFields = null;
		
		if(hashSourceFieldName.equals("ip-dst-port")) {
			hashSourceFieldName="ip-dst|port";
			destinationFields = sha256FieldAlias.split("\\|");
		}
		
		boolean generateSha256 = IocUtils.needsSha256Conversion(hashSourceFieldName);
		
		String eventsIndexName = parameters.get("indexPattern");
		
		String esNodesStr = parameters.get("esNodes");
		
		List<String> esNodes = Arrays.asList(esNodesStr.split(","));
		
		String bloomFilterInputPath = parameters.get("bloomInput");
		
		String iocIndexName = parameters.get("iocIndexName");
		
		String truePositivesIndexName = parameters.get("truePositivesIndexName");
		
		String sparkMasterUrl =  parameters.get("sparkMaster");
		
		String driverMemory = parameters.get("driverMemory");
		
		String executorMemory = parameters.get("executorMemory");
		
		String truePositivesDetectionWindow = parameters.get("truePositivesDetectionWindow");

		SparkConf conf = new SparkConf().setAppName(iocId+" hashes detection").setMaster(sparkMasterUrl).set("spark.driver.memory",driverMemory);

		conf.set("es.nodes", esNodesStr);
		
		if(hashSourceFieldName.equals("ip-dst-port")) {
			conf.set("es.query", getMultiFieldsQuery(destinationFields, truePositivesDetectionWindow));
			conf.set("es.read.field.include", String.join(",", destinationFields));//+",sas.id");
		}else {

			conf.set("es.query", getQuery(sha256FieldAlias,truePositivesDetectionWindow));
			conf.set("es.read.field.include", sha256FieldAlias);//+",sas.id");
		}
		
		
		
		
		
		conf.set("es.input.max.docs.per.partition","10000");

		JavaSparkContext jsc = null;

		Long falsePositivesNumber = null;
		Long truePositivesNumber = null;
		Long eventsTested=null;
		try {
			jsc = new JavaSparkContext(conf);

			LongAccumulator events = new LongAccumulator();
			jsc.sc().register(events, "eventsTested");
			
			LongAccumulator falsePositives = new LongAccumulator();
			jsc.sc().register(falsePositives, "bloomFilterFalsePositives");
			
			LongAccumulator truePositives = new LongAccumulator();
			jsc.sc().register(truePositives, "bloomFilterTruePositives");
			
			BloomFilter f = BloomFilter.readFrom(new FileInputStream(new File(bloomFilterInputPath)));
			
			Broadcast<BloomFilter> filter = jsc.broadcast(f);

			logger.info("bloom filter loaded");

			JavaPairRDD<String, Map<String, Object>> esRDD = JavaEsSpark.esRDD(jsc, eventsIndexName);

			//long events = esRDD.count();

			//logger.info("Loading {}  events with hashes", events);

			long start = System.currentTimeMillis();

			BloomFilterFunction filterFunction = new BloomFilterFunction(events);

			filterFunction.setShaFieldName(sha256FieldAlias);

			filterFunction.setFilter(filter);
			
			filterFunction.setGenerateSha256(generateSha256);
			
			Optional<List<String>> deprecatedHashes = getDeprecatedHashes(hashSourceFieldName);
			if(deprecatedHashes.isPresent()) {
				
				Broadcast<List<String>> excludedHashes = jsc.broadcast(deprecatedHashes.get());
				
				filterFunction.setExcludedHashes(excludedHashes);
			}

			JavaPairRDD<String, Map<String, Object>> positiveCandidates = esRDD.filter(filterFunction);
			
			//long positiveCandidatesNumber = positiveCandidates.count();

			//logger.info(" {}  positive candidates", positiveCandidatesNumber);
			JavaRDD<Map<String, Object>> truePositivesRdd = positiveCandidates.mapPartitions(new CheckIocPositivesFunction(iocIndexName,
																															sha256FieldAlias,
																															eventsIndexName,
																															generateSha256,
																															esNodes,
																															hashSourceFieldName,
																															truePositives,
																															falsePositives));

			// long candidatesNumber = positiveCandidates.count();

			// long positivesNumber = truePositives.count();

			JavaEsSpark.saveToEs(truePositivesRdd, truePositivesIndexName);

			double duration = (System.currentTimeMillis() - start) / 1000L;

			 falsePositivesNumber = falsePositives.count();
			 truePositivesNumber = truePositives.count();
			 eventsTested = events.count();
			 
			logger.info("found {} bloom-filter false positives and {} tru positives Over {} tested events, in {} seconds",falsePositivesNumber,truePositivesNumber,eventsTested,duration);
			 
			saveStatistics(jsc, iocId, truePositivesNumber, falsePositivesNumber, eventsTested);

		} catch (Throwable e) {

			logger.error("Failed to detect iocs  with bloom filter", e);
			throw new RuntimeException(e);
		} finally {

			jsc.stop();

		}
		
		
	}
	
	private static void saveStatistics(JavaSparkContext jsc,String iocId,long truePositives,long falsePositives,long eventsTested) {
		Map<String,Object> stat = new HashMap<>();
		stat.put("ioc",iocId);
		stat.put("bloomfilter", truePositives);
		stat.put("logs", eventsTested);
		stat.put("@timestamp", System.currentTimeMillis());
		stat.put("true_positives", truePositives);
		stat.put("false_positives", falsePositives);
		List<Map<String,Object>> stats = new ArrayList<>();
		stats.add(stat);
		JavaRDD<Map<String,Object>> statsRdd = jsc.parallelize(stats);
		
		JavaEsSpark.saveToEs(statsRdd, "iocs-test-statistics");
		
	}
	
	private static Optional<List<String>> getDeprecatedHashes(String attributeType){
		Optional<List<String>> deprecatedHashes=Optional.empty();
		Optional<SparkClientConfig> config = SparkClient.loadConfig();

		if (config.isPresent()) {
			try {
				MispConfig misp = config.get().getMisp();

				//MispClient mispCli = new MispClient(misp);

				
				//deprecatedHashes = mispCli.listDeprecatedAttributes(attributeType);
			} catch (Exception e) {
				logger.error("Faikled to instantiate Misp client", e);
			}

		}

		return deprecatedHashes;
	}
	
	private static String getQuery(String sha256FieldAlias,String truePositivesDetectionWindow) {
		String query = "{\n" + "  \"query\": {\n" + "   \"bool\":{\n" + "      \"filter\": [\n"
				+ "        { \"range\": { \"@timestamp\": { \"gte\": \"DETECTION_WINDOW\" }}},\n" + "        {\"exists\": {\n"
				+ "          \"field\": \"SHA256_ALIAS\"\n" + "        }}\n" + "     ]\n" + "  }\n" + "  },\n"
				+ "  \"fields\": [\"SHA256_ALIAS\"]\n}";// + "  ,\"_source\": false\n" + "}";
		query =  query.replaceAll("SHA256_ALIAS", sha256FieldAlias).replaceAll("DETECTION_WINDOW", truePositivesDetectionWindow);
		logger.info(query);

		return query;
	}

	private static String getMultiFieldsQuery(String[] fieldNames,String truePositivesDetectionWindow) {
		String query = "{\n" + "  \"query\": {\n" + "   \"bool\":{\n" + "      \"filter\": [\n"
				+ "        { \"range\": { \"@timestamp\": { \"gte\": \"DETECTION_WINDOW\" }}},\n" + "        {\"exists\": {\n"
				+ "          \"field\": \"SHA256_ALIAS\"\n" + "        }}\n" + "     ]\n" + "  }\n" + "  },\n"
				+ "  \"fields\": [\"FIELD1\",\"FIELD2\"]\n}";// + "  ,\"_source\": false\n" + "}";
		query =  query.replaceAll("SHA256_ALIAS", fieldNames[1]).replaceAll("DETECTION_WINDOW", truePositivesDetectionWindow).replaceAll("FIELD1", fieldNames[0]).replaceAll("FIELD2", fieldNames[1]);
		logger.info(query);

		return query;
	}
	public static class BloomFilterFunction implements Function<Tuple2<String, Map<String, Object>>, Boolean>,Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private static transient Logger logger = LoggerFactory.getLogger(BloomFilterFunction.class);

		public Broadcast<BloomFilter> bloomFilter;
		
		private Broadcast<List<String>> excludedHashes;
		
		//private LongAccumulator positiveCounter;
		public boolean generateSha256;
		public String shaFieldName;
		private LongAccumulator eventsTested;
		
		public BloomFilterFunction(LongAccumulator eventsTested) {
			this.eventsTested=eventsTested;

		}

		public void setShaFieldName(String shaFieldName) {
			this.shaFieldName = shaFieldName;

		}
		
		public void setFilter(Broadcast<BloomFilter> aFilter) {
			this.bloomFilter = aFilter;
		}
		
		

		public void setExcludedHashes(Broadcast<List<String>> excludedHashes) {
			this.excludedHashes = excludedHashes;
		}

		public void setGenerateSha256(boolean generateSha256) {
			this.generateSha256 = generateSha256;
		}

		public Boolean call(Tuple2<String, Map<String, Object>> tuple) throws Exception {

			boolean isCandidate = false;

			Map<String, Object> candidateDocWithSha256 = tuple._2;

			String sha256 = getSha256(shaFieldName, candidateDocWithSha256);

			if (sha256 != null) {
				eventsTested.add(1L);
				//here we exclude the hashes marked as deprecated in misp
				if(excludedHashes!=null && excludedHashes.getValue().contains(sha256)) {
					return false;
				}

				boolean isPositive = bloomFilter.value().mightContainString(sha256);

				if (isPositive) {

					isCandidate = isPositive;
					candidateDocWithSha256.put("elastioc", sha256);
					
				}
					 
			}else {
				logger.error("document should have {} field",shaFieldName);
			}

			return isCandidate;
		}

		private String getSha256(String sha256FieldName, Map<String, Object> candidate) {
			String sha256 = null;
			
			if (sha256FieldName != null) {
				
				sha256 = (String) candidate.get(sha256FieldName);
				if (sha256 == null) {
					Optional<Object> optSha256 = getOptionalValue(sha256FieldName, candidate, false);
					if (optSha256.isPresent()) {
						sha256 = (String) optSha256.get();

						
					}
				}
				
				if(generateSha256 && sha256!=null) {
					sha256 = Hashing.sha256().hashString((String)sha256, StandardCharsets.UTF_8).toString();
					
				}
			}

			return sha256;
		}

		public Optional<Object> getOptionalValue(String fieldName, Map<String, Object> entity, boolean... withRemoval) {

			boolean remove = (withRemoval != null && withRemoval.length == 1) ? withRemoval[0] : false;

			Optional<Object> value = Optional.empty();

			String[] splittedFieldName = fieldName.split("\\.");

			for (int i = 0; i < splittedFieldName.length; i++) {

				String key = splittedFieldName[i];

				if (entity.containsKey(key)) {

					value = Optional.ofNullable(entity.get(key));

					if (i < splittedFieldName.length - 1) {
						if (!value.isPresent()) {
							break;
						}
						entity = (Map<String, Object>) value.get();
					} else if (remove) {
						entity.remove(key);
					}

				} else {

					value = Optional.empty();
					break;
				}

			}

			return value;

		}

		private Object getFieldValue(String fieldKey, Map<String, Object> container) {
			Object fieldValue = null;

			return fieldValue;
		}

	}

}
