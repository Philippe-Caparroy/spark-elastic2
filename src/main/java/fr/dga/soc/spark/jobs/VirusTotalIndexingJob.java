/**
 * 
 */
package fr.dga.soc.spark.jobs;

import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.dga.soc.spark.SparkParameterTool;
import fr.dga.soc.spark.functions.VTIndexingFunction;

/**
 * @author pcaparroy
 *
 */
public class VirusTotalIndexingJob {

	private static Logger logger = LoggerFactory.getLogger(VirusTotalIndexingJob.class);

	/**
	 * 
	 */
	public VirusTotalIndexingJob() {

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		SparkParameterTool parameters = SparkParameterTool.fromArgs(args);
		
		
		String parquetInputDir = parameters.get("inputDir");
		String elasticsearchNodes = parameters.get("esNodes");
		String indexName = parameters.get("vtIndexName");
		
		if(parquetInputDir ==null || elasticsearchNodes==null) {
			
			logger.info("you must provide input dir and elasticsearch host");
		
		}else {
		
		SparkConf conf = new SparkConf().setAppName("VirusTotalIndexing").setMaster("local[*]");

		conf.set("es.nodes", elasticsearchNodes);

		JavaSparkContext jsc = new JavaSparkContext(conf);

		
		long start = System.currentTimeMillis();
		SparkSession spark = SparkSession.builder().sparkContext(jsc.sc()).getOrCreate();
		try {
		

			Dataset<Row> vts = spark.read().option("mergeSchema", "true").parquet(parquetInputDir + "/*");
			
			JavaRDD<Map<String,Object>> docs = vts.javaRDD().map(new VTIndexingFunction());

			JavaEsSpark.saveToEs(docs, indexName);
			
			double duration = (System.currentTimeMillis() - start) / 1000;
			
			logger.info("duration(s): " + duration);


		} catch (Exception e) {

			logger.error("Failed to index Virus total hashes",e);;

		} finally {
			
			spark.close();
		
		}


	}
	}
}
