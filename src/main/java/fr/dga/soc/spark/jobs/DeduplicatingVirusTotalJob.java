/**
 * 
 */
package fr.dga.soc.spark.jobs;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.concat_ws;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.util.LongAccumulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.dga.soc.VtJob;
import fr.dga.soc.spark.SparkParameterTool;
import fr.dga.soc.spark.functions.DeduplicateFunction;

/**
 * @author pcaparroy
 *
 */
public class DeduplicatingVirusTotalJob {

	private static Logger logger = LoggerFactory.getLogger(DeduplicatingVirusTotalJob.class);

	/**
	 * 
	 */
	public DeduplicatingVirusTotalJob() {
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SparkParameterTool parameters = SparkParameterTool.fromArgs(args);
		
		
		String parquetInputDir = parameters.get("inputDir");
		String parquetOutputDir = parameters.get("outputDir");
				
		logger.info("Removing eventual VT hashes triples duplicates");
		long start = System.currentTimeMillis();
		SparkConf conf = new SparkConf().setAppName("VT hashes duplic&te removal").setMaster("local[*]");
		JavaSparkContext jsc = new JavaSparkContext(conf);

		SparkSession spark = SparkSession.builder().sparkContext(jsc.sc()).getOrCreate();
		
		try {
			Path baseDirectory = Paths.get(new URI("file://"+parquetInputDir));
			List<String> subpaths = Files.walk(baseDirectory,1).filter(Files::isDirectory).map(path->path.toAbsolutePath().toString()).collect(Collectors.toList());
			subpaths.remove(baseDirectory.toAbsolutePath().toString());
			
			//subpaths = subpaths.stream().filter(str->str.endsWith("_parquet")).collect(Collectors.toList());
			
			//.filter(p->{System.out.println(p.toAbsolutePath().toString());return p.toAbsolutePath().toString().endsWith("pqt");})
			
			  Dataset<Row> data = spark.read().parquet(subpaths.toArray(new
			  String[subpaths.size()])).filter(col("av_positives").gt(4)).select(col("md5")
			  , col("sha1"), col("sha256"),col("av_positives"), col("av_tested"),col("fname"),
			  col("record_date")).withColumn("hashTriple",
			  concat_ws("/",col("md5"),col("sha1"),col("sha256")));
			  LongAccumulator duplicatesCounter = jsc.sc().longAccumulator();
			  
			  JavaRDD<Row> duplicates =data.javaRDD().keyBy(row->(String)row.getAs("hashTriple")).groupByKey().map(
			  new DeduplicateFunction(duplicatesCounter));
			  
			  //logger.info("Found {} hashes triple duplicates",duplicateHashesTriples);
			  spark.sqlContext().createDataFrame(duplicates,
			  data.schema()).drop(col("hashTriple")).write().parquet(parquetOutputDir);
			  
			  logger.info("{} duplicates found",duplicatesCounter.value());
		 
			
		} catch (Exception e) {
			logger.error("failed to deduplicate VT hashes:",e);
		}finally {
			spark.close();
		}

	}

}
