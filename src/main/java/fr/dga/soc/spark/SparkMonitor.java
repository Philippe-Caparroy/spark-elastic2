/**
 * 
 */
package fr.dga.soc.spark;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.spark.launcher.SparkAppHandle;
import org.apache.spark.launcher.SparkLauncher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * @author pcaparroy
 *
 */
public class SparkMonitor {

	Logger logger = LoggerFactory.getLogger(SparkMonitor.class);


	private boolean sparkJobRunning;
	private boolean sparkJobFinished;
	private boolean sparkJobFailed;
	/**
	 * 
	 */
	public SparkMonitor() {

	}


	public void monitorJob(SparkLauncher sparkLauncher,String jobName) {

		try {
			;
			sparkLauncher.redirectToLog(createLogger("Spark-batch-job"));

			System.setProperty("java.util.logging.SimpleFormatter.format", "%5$s%6$s%n");
			// Launches a sub-process that will start the configured Spark application.
			SparkAppHandle sparkProcess = sparkLauncher.startApplication();
			
			

			 boolean jobRunning = true;

	        while (jobRunning) {
	            
	            monitorJobExecution(jobName,sparkProcess);
	            
	           

	            if (sparkJobFailed || sparkJobFinished) {

	                jobRunning = false;
	                
	                sparkProcess.kill();
	            }
	        }
		        

			//logger.info(message, currentState);
		} catch (Exception e) {
			
			logger.error("Failed to monitor job {}",jobName,e);
		}

	}

	private boolean monitorJobExecution(String sparkJobName, SparkAppHandle sparkProcess)
	{

		if (sparkJobFinished) {
			return false;
		}
		try {
			Thread.sleep(500);

		} catch (InterruptedException e) {

		}
		boolean running = true;
		//  String jobAppId = sparkProcess.getAppId();
		String currentState = sparkProcess.getState().toString().toLowerCase();

		String message = "Spark " + sparkJobName + " {}";

		switch (currentState) {
		case "finished":

			logger.info(message, currentState);
			running = false;
			
			sparkJobFinished = true;

			break;
		case "failed":
			logger.error(message, currentState);
			
			running = false;
			sparkJobFailed = true;
			break;
		case "killed":
			logger.warn(message, currentState);
			
			running = false;
			sparkJobFailed = true;
			break;
		case "running":
			if (!sparkJobRunning) {
				logger.info(message, currentState);
				
				sparkJobRunning = true;
			}

			break;
		default:
			// logger.info(message,currentState);
			break;
		}

		

		return running;
	}

	/**
	 * Creates the logger.
	 *
	 * @param appName the app name
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private String createLogger(String appName) throws IOException {
		final java.util.logging.Logger appLogger = getRootLogger();

		final FileHandler handler = new FileHandler("/var/log/elastioc/" + appName + "-%u-%g.log", 10_000_000, 5,
				true);
		handler.setFormatter(new SimpleFormatter());
		appLogger.addHandler(handler);
		appLogger.setLevel(java.util.logging.Level.INFO);

		return appLogger.getName();
	}

	private java.util.logging.Logger getRootLogger() {
		final java.util.logging.Logger rootLogger = java.util.logging.Logger
				.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME);
		java.util.Arrays.stream(rootLogger.getHandlers()).forEach(rootLogger::removeHandler);
		// Without this the logging will go to the Console and to a file.
		rootLogger.setUseParentHandlers(false);
		return rootLogger;
	}


}
