/**
 * 
 */
package fr.dga.soc.spark.functions;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.function.FlatMapFunction;

import com.google.common.hash.Hashing;

/**
 * @author pcaparroy
 *
 */
public class Doc2IocFunction implements FlatMapFunction<Iterator<Map<String, Object>>, Map<String,String>> {

	
	private String hashFieldName;
	private boolean generateSha256;
	/**
	 * 
	 */
	public Doc2IocFunction(String hashFieldName,boolean generateSha256) {
		this.hashFieldName=hashFieldName;
		this.generateSha256=generateSha256;
	}
	@Override
	public Iterator<Map<String,String>> call(Iterator<Map<String, Object>> t) throws Exception {
		List<Map<String,String>> hashValues = new ArrayList<>();
		
		while (t.hasNext()) {
			Map<java.lang.String, java.lang.Object> map = (Map<java.lang.String, java.lang.Object>) t.next();
			
			String hashValue = (String)map.get(hashFieldName);
			if(generateSha256) {
				hashValue = Hashing.sha256().hashString(hashValue, StandardCharsets.UTF_8).toString();
			}
			Map<String,String> hash = new HashMap<>();
			hash.put(hashFieldName, hashValue);
			hashValues.add(hash);
			
		}
		
		return hashValues.iterator();
	}

}
