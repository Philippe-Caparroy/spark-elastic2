/**
 * 
 */
package fr.dga.soc.spark.functions;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;

import scala.collection.JavaConversions;

/**
 * @author pcaparroy
 *
 */
public class FakeVTIndexingFunction implements Function<Row, Map<String, Object>> {

	private List<String> fieldNames;
	/**
	 * 
	 */
	public FakeVTIndexingFunction() {
		
		fieldNames = Arrays.asList("sha1","sha256","md5");
		
		
	}

	@Override
	public Map<String, Object> call(Row row) throws Exception {

		Map<String,Object> doc = new HashMap<String,Object>();//(Map<String, Object>) row.getValuesMap(JavaConversions.asScalaBuffer(fieldNames));
		
		doc.put("sha256", (String)row.getAs("sha256"));
		/*
		 * doc.put("sha1", (String)row.getAs("sha1")); doc.put("md5",
		 * (String)row.getAs("md5"));
		 */	
		String day =  (String)row.getAs("record_date");
		
		long timestamp = LocalDateTime.parse(day, DateTimeFormatter.ISO_DATE).toEpochSecond(ZoneOffset.UTC)*1000;
		doc.put("date",new Date(timestamp));
		doc.put("av_positives", (Integer)row.getAs("av_positives"));
		doc.put("av_tested", (Integer)row.getAs("av_tested"));
		doc.put("fname", (Integer)row.getAs("fname"));
		//if(row.getAs(""))
		
		return doc;
	}

}
