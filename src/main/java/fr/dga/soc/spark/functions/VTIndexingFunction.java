/**
 * 
 */
package fr.dga.soc.spark.functions;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;

/**
 * @author pcaparroy
 *
 */
public class VTIndexingFunction implements Function<Row, Map<String, Object>> {

	private List<String> fieldNames;
	/**
	 * 
	 */
	public VTIndexingFunction() {
		
		
		
	}

	@Override
	public Map<String, Object> call(Row row) throws Exception {

		Map<String,Object> doc = new HashMap<String,Object>();
		
		doc.put("sha256", (String)row.getAs("sha256"));
		
		doc.put("date", (Date)row.getAs("record_date"));
		doc.put("av_positives", (Integer)row.getAs("av_positives"));
		doc.put("av_tested", (Integer)row.getAs("av_tested"));
		doc.put("fname", (String	)row.getAs("fname"));
		
		return doc;
	}

}
