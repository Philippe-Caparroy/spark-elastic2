/**
 * 
 */
package fr.dga.soc.spark.functions;

import java.util.Collection;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;
import org.apache.spark.util.LongAccumulator;

import scala.Tuple2;

/**
 * @author pca
 *
 */




public class DeduplicateFunction implements Function<Tuple2<String, Iterable<Row>>, Row> {

	private LongAccumulator duplicatesCounter;
	
	public DeduplicateFunction(LongAccumulator duplicatesCounter) {
		
		this.duplicatesCounter=duplicatesCounter;
		
	}

	
	@Override
	public Row call(Tuple2<String,Iterable<Row>> tuple) throws Exception {
		Collection<Row> duplicates = (Collection<Row>)tuple._2;
		if(duplicates.size()>1) {
			duplicatesCounter.add(1);
		}
		
		
		return duplicates.iterator().next();
	}

}
