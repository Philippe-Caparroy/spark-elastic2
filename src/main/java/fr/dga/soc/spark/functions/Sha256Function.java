/**
 * 
 */
package fr.dga.soc.spark.functions;

import java.nio.charset.StandardCharsets;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;

import com.google.common.hash.Hashing;


/**
 * @author pcaparroy
 *
 */
public class Sha256Function implements Function<Row, Sha256> {

	public String fieldName;
	/**
	 * 
	 */
	public Sha256Function(String fieldName) {
		this.fieldName=fieldName;
	}

	
	@Override
	public Sha256 call(Row v1) throws Exception {
		String originalValue = v1.getString(0);
		
		String sha256Str = Hashing.sha256().hashString(originalValue, StandardCharsets.UTF_8).toString();
		
		Sha256 sha256 = new Sha256(sha256Str,fieldName,originalValue);
		return sha256;
	}

}
