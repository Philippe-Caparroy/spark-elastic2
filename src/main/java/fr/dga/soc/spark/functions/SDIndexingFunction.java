/**
 * 
 */
package fr.dga.soc.spark.functions;

import java.sql.Date;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;

import scala.collection.JavaConversions;

/**
 * @author pcaparroy
 *
 */
public class SDIndexingFunction implements Function<Row, Map<String, Object>> {

	private List<String> fieldNames;
	/**
	 * 
	 */
	public SDIndexingFunction(List<String> fieldNames) {
		
		this.fieldNames=fieldNames;
		
	}

	@Override
	public Map<String, Object> call(Row row) throws Exception {

		Map<String,Object> doc = new HashMap<String,Object>();
		
		for(int i=0;i<fieldNames.size();i++) {
			doc.put(fieldNames.get(i), row.get(i));
		}
		
		/*doc.put("sha256", (String)row.getAs("sha256"));
		doc.put("sha256", (String)row.getAs("sha256"));
		doc.put("date", (Date)row.getAs("record_date"));
		doc.put("av_positives", (Integer)row.getAs("av_positives"));
		doc.put("av_tested", (Integer)row.getAs("av_tested"));*/
		
		
		return doc;
	}

}
