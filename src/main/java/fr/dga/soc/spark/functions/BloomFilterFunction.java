/**
 * 
 */
package fr.dga.soc.spark.functions;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.util.sketch.BloomFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import scala.Tuple2;

/**
 * @author pca
 *
 */
public class BloomFilterFunction implements Function<Tuple2<String, Map<String, Object>>, Boolean>,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static transient Logger logger = LoggerFactory.getLogger(BloomFilterFunction.class);

	public Broadcast<BloomFilter> bloomFilter;
	//private LongAccumulator positiveCounter;
	public String shaFieldName;

	public BloomFilterFunction() {
		
	}

	public void setShaFieldName(String shaFieldName) {
		this.shaFieldName = shaFieldName;

	}
	
	public void setFilter(Broadcast<BloomFilter> aFilter) {
		this.bloomFilter = aFilter;
	}

	public Boolean call(Tuple2<String, Map<String, Object>> tuple) throws Exception {

		boolean isCandidate = false;

		Map<String, Object> candidateDocWithSha256 = tuple._2;

		String sha256 = getSha256(shaFieldName, candidateDocWithSha256);

		if (sha256 != null) {

			boolean isPositive = bloomFilter.value().mightContainString(sha256);

			if (isPositive) {

				isCandidate = isPositive;
			}
		}

		return isCandidate;
	}

	private String getSha256(String sha256FieldName, Map<String, Object> candidate) {
		String sha256 = null;
		Object value = null;
		if (sha256FieldName != null) {
			/*
			 * String[] objFields = sha256FieldName.split("\\."); for (int i = 0; i <
			 * objFields.length; i++) { String fieldKey = objFields[i];
			 * if(i<objFields.length-1) { if(value==null) { value = candidate.get(fieldKey);
			 * }else { value = ((Map<String,Object>)value).get(fieldKey); } }else { sha256 =
			 * (String)((Map<String,Object>)value).get(fieldKey); } }
			 */
			sha256 = (String) candidate.get(sha256FieldName);
			if (sha256 == null) {
				Optional<Object> optSha256 = getOptionalValue(sha256FieldName, candidate, false);
				if (optSha256.isPresent()) {
					sha256 = (String) optSha256.get();
				}
			}
		}

		return sha256;
	}

	public Optional<Object> getOptionalValue(String fieldName, Map<String, Object> entity, boolean... withRemoval) {

		boolean remove = (withRemoval != null && withRemoval.length == 1) ? withRemoval[0] : false;

		Optional<Object> value = Optional.empty();

		String[] splittedFieldName = fieldName.split("\\.");

		for (int i = 0; i < splittedFieldName.length; i++) {

			String key = splittedFieldName[i];

			if (entity.containsKey(key)) {

				value = Optional.ofNullable(entity.get(key));

				if (i < splittedFieldName.length - 1) {
					if (!value.isPresent()) {
						break;
					}
					entity = (Map<String, Object>) value.get();
				} else if (remove) {
					entity.remove(key);
				}

			} else {

				value = Optional.empty();
				break;
			}

		}

		return value;

	}

	private Object getFieldValue(String fieldKey, Map<String, Object> container) {
		Object fieldValue = null;

		return fieldValue;
	}

}
