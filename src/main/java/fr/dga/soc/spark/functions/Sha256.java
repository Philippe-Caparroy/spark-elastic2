/**
 * 
 */
package fr.dga.soc.spark.functions;

/**
 * @author pcaparroy
 *
 */
public class Sha256{
	
	public String sha256;
	public String sourceFieldName;
	public String sourceFieldValue;
	
	
	
	public Sha256() {
		super();
	}



	public Sha256(String sha256Str,String fieldName,String strValue) {
		this.sourceFieldName=fieldName;
		this.sourceFieldValue=strValue;
		this.sha256=sha256Str;
	}



	public String getSha256() {
		return sha256;
	}



	public void setSha256(String sha256) {
		this.sha256 = sha256;
	}



	public String getSourceFieldName() {
		return sourceFieldName;
	}



	public void setSourceFieldName(String sourceFieldName) {
		this.sourceFieldName = sourceFieldName;
	}



	public String getSourceFieldValue() {
		return sourceFieldValue;
	}



	public void setSourceFieldValue(String sourceFieldValue) {
		this.sourceFieldValue = sourceFieldValue;
	}
	
	
}