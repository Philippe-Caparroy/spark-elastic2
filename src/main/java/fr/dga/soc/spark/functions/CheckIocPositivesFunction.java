/**
 * 
 */
package fr.dga.soc.spark.functions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.http.HttpHost;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.Row;
import org.apache.spark.util.LongAccumulator;
import org.apache.spark.util.sketch.BloomFilter;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import scala.Tuple2;

/**
 * @author pca
 *
 */
public class CheckIocPositivesFunction implements FlatMapFunction<Iterator<Tuple2<String,Map<String,Object>>>, Map<String,Object>> {
	
	private static Logger logger = LoggerFactory.getLogger(CheckIocPositivesFunction.class);
	
	
	private String iocsIndexName;
	private String sourceEventsIndexName;
	
	private RestHighLevelClient elasticClient;
	private String sha256FieldName;
	public boolean generateSha256;
	private List<String> esNodes;
	private String hashSourceFieldName;
	private LongAccumulator falsePositives;
	private LongAccumulator truePositives;
	
	public CheckIocPositivesFunction(String iocsIndexName,
									String sha256FieldName,
									String eventsIndexName,
									boolean generateSha256,
									List<String> esNodes,
									String hashSourceFieldName,
									LongAccumulator truePositives,
									LongAccumulator falsePositives
									) {
		this.iocsIndexName = iocsIndexName;
		this.sha256FieldName=sha256FieldName;
		this.sourceEventsIndexName = sourceEventsIndexName;
		this.generateSha256=generateSha256;
		this.esNodes=esNodes;
		this.hashSourceFieldName=hashSourceFieldName;
		this.falsePositives=falsePositives;
		this.truePositives=truePositives;
		
	}
	
	private Map<String,Object> getTruePositive(RestHighLevelClient client,String iocFieldName,String value) {
		
		Map<String,Object> truePositive = null;
		
		try {
			
			
			SearchRequest searchRequest = new SearchRequest(iocsIndexName); 
			
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			
			//logger.info(sha256);
			
			searchSourceBuilder.query(QueryBuilders.termQuery(iocFieldName, value));
			
			searchRequest.source(searchSourceBuilder); 
			
			SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
			
			
			if(response.getHits().getTotalHits().value>0) {
				
				SearchHits hits = response.getHits();
				 truePositive = hits.getAt(0).getSourceAsMap();
			}else {
				
			}

		} catch (IOException e) {
			logger.error("Failed to check candidate positive",e);
		}
		
		
		
		return truePositive;
	}


	

	@Override
	public Iterator<Map<String, Object>> call(Iterator<Tuple2<String,Map<String, Object>>> candidatePositivesIterator) throws Exception {
		
		List<Map<String,Object>> truePositivesCollector = new ArrayList<>();
		
		RestHighLevelClient elasticClient = null;
		
		try {
			elasticClient = new RestHighLevelClient(RestClient.builder(new HttpHost(esNodes.get(0), 9200, "http")));
			
			while (candidatePositivesIterator.hasNext()) {
				
				Tuple2<String,Map<java.lang.String, java.lang.Object>> tuple = (Tuple2<String,Map<java.lang.String, java.lang.Object>>) candidatePositivesIterator.next();
				
				Map<String,Object> candidate = tuple._2;
				String eventId = tuple._1;
				
				String sha256 = (String)candidate.get(sha256FieldName);
				String iocFieldName=hashSourceFieldName;
				if(generateSha256) {
					sha256=(String)candidate.get("elastioc");
					iocFieldName="ioc_sha256";
				}
				if(sha256==null) {
					Optional<Object> optSha256 = getOptionalValue(sha256FieldName, candidate, false);
					if(optSha256.isPresent()) {
						sha256 = (String)optSha256.get();
					}
				}
				
				Map<String,Object> truePositive = getTruePositive(elasticClient,iocFieldName, sha256);
				
				if(truePositive != null) {
					
					enrichTruePositive(truePositive,eventId, candidate, sourceEventsIndexName);
					truePositivesCollector.add(truePositive);
					this.truePositives.add(1L);
					//break;
				
				}else {
					
					falsePositives.add(1L);
				}
				
			}
			
			
		} catch (Exception e) {
			
			logger.error("Failed to check true positives",e);

		}finally {
			if(elasticClient!=null) {
				try {
					
					elasticClient.close();

					
				} catch (Exception e2) {
					logger.error("Failed to close Elasticsearch client",e2);
				}
			}
		}
		
		
		return truePositivesCollector.iterator();
	}
	
	private void enrichTruePositive(Map<String,Object> truePositive,String eventId,Map<String,Object> sourceEvent,String sourceIndexName) {
		
		truePositive.put("event.id", eventId);
		
		truePositive.put("@timestamp", new Date(System.currentTimeMillis()));
		
		addSourceEventFieldValue(sourceEvent, "sas.id", truePositive);
	}

	private void addSourceEventFieldValue(Map<String,Object> sourceEvent,String sourceEventFieldName,Map<String,Object> truePositive) {
		
		Optional<Object> optionalFieldValue = getOptionalValue(sourceEventFieldName, sourceEvent, false);
		if(optionalFieldValue.isPresent()) {
			truePositive.put(sourceEventFieldName, optionalFieldValue.get());
		}
		
	}

public Optional<Object> getOptionalValue(String fieldName, Map<String, Object> entity,boolean...withRemoval) {
        
        boolean remove = (withRemoval!=null&& withRemoval.length==1)?withRemoval[0]:false;
      
        Optional<Object>  value = Optional.empty();
        
        String[] splittedFieldName = fieldName.split("\\.");
        
        for (int i = 0; i < splittedFieldName.length; i++) {
        
            String key = splittedFieldName[i];
            
            if(entity.containsKey(key)) {
                
                value = Optional.ofNullable(entity.get(key));
                
                if (i < splittedFieldName.length - 1) { 
                    if (!value.isPresent()) { 
                        break; 
                     } 
                    entity= (Map<String, Object>) value.get(); 
                }else if(remove){
                    entity.remove(key);
                }
                
            }else {
                
                value = Optional.empty();
                break;
            }
            
        
        }


      return value;

  }

}
