/**
 * 
 */
package fr.dga.soc.spark.functions;

import java.sql.Date;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;

import scala.collection.JavaConversions;

/**
 * @author pcaparroy
 *
 */
public class EventIndexingFunction implements Function<Row, Map<String, Object>> {

	/**
	 * 
	 */
	public EventIndexingFunction() {
		
		
		
	}

	@Override
	public Map<String, Object> call(Row row) throws Exception {

		Map<String,Object> doc = new HashMap<String,Object>();
		Map<String,Object> hash = new HashMap<>();
		hash.put("sha256", (String)row.getAs("sha256"));
		doc.put("hash", hash);
		doc.put("@timestamp", System.currentTimeMillis());
		;
		
		
		return doc;
	}

}
