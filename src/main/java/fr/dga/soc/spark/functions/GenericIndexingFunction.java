/**
 * 
 */
package fr.dga.soc.spark.functions;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;

import com.google.common.hash.Hashing;

/**
 * @author pcaparroy
 *
 */
public class GenericIndexingFunction implements Function<Row, Map<String,Object>> {

	public String[] sourceFieldNames;
	public boolean generateSha256;
	public String hashSourceFieldName;
	private String hashFieldName;
	/**
	 * 
	 */
	public GenericIndexingFunction(String hashFieldName,String hashSourceFieldName,String[] sourceFieldNames,boolean generateSha256) {
		this.hashFieldName=hashFieldName;
		this.hashSourceFieldName=hashSourceFieldName;
		this.generateSha256=generateSha256;
		this.sourceFieldNames=sourceFieldNames;
	}

	@Override
	public Map<String, Object> call(Row v1) throws Exception {
		
		Map<String,Object> item = new HashMap<>();
		
		for(String fieldName:sourceFieldNames) {
			
			Object fieldValue = v1.getAs(fieldName);
			item.put(fieldName, fieldValue);
		
			if(generateSha256 && hashSourceFieldName.equals(fieldName)) {
				String hashValue = Hashing.sha256().hashString((String)fieldValue, StandardCharsets.UTF_8).toString();
				item.put(hashFieldName, hashValue);
			}
		}
		return item;
	}

}
