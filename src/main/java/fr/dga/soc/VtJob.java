/**
 * 
 */
package fr.dga.soc;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.Dataset;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;
import org.apache.spark.sql.Row;
import static org.apache.spark.sql.functions.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.spark.sql.SparkSession;
import org.apache.spark.util.LongAccumulator;
import org.apache.spark.util.sketch.BloomFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.dga.soc.spark.SparkParameterTool;
import fr.dga.soc.spark.functions.BloomFilterFunction;
import fr.dga.soc.spark.functions.CheckIocPositivesFunction;
import fr.dga.soc.spark.functions.DeduplicateFunction;
import fr.dga.soc.spark.functions.EventIndexingFunction;
import fr.dga.soc.spark.functions.FakeVTIndexingFunction;
import fr.dga.soc.spark.functions.VTIndexingFunction;
import scala.Tuple2;

/**
 * @author pca
 *
 */
public class VtJob {

	private static Logger logger = LoggerFactory.getLogger(VtJob.class);

	public static void main(String[] args) {

		logger.info("Starting...");
		
		SparkParameterTool sparkParameters = SparkParameterTool.fromArgs(args);

		String parquetInputDir = sparkParameters.get("inputDir");

		String parquetOutputDir = sparkParameters.get("outputDir");

		String bloomOutput = sparkParameters.get("bloomOutput");

		String withStatistics = sparkParameters.get("stats");

		String bloomFilterInputPath = sparkParameters.get("bloomInput");
		
		String fakeBloomFilterInputPath = sparkParameters.get("fakeBloomInput");

		
		String indexName = sparkParameters.get("indexName");

		String vtIndexName = sparkParameters.get("vtIndexName");
		
		String matchBloomFilter=sparkParameters.get("matchBloomFilter");
		
		String sha256FieldName = sparkParameters.get("sha256FieldName");
		
		String generateFakeEvents = sparkParameters.get("generateFakeEvents");
		
		String indexFakeData = sparkParameters.get("indexFakeVtData");

		boolean statistics = withStatistics != null ? Boolean.valueOf(withStatistics) : false;
		
		if(indexName!=null && parquetInputDir!=null && indexFakeData==null) {
			logger.info("Indexing vt data");

			virusTotalIndexing(parquetInputDir, indexName);
			
		}else if (parquetInputDir != null && generateFakeEvents != null) {
			logger.info("Generating fake events with hashes");

			fakeEventsWithHashIndexing(parquetInputDir);

		}else if (parquetInputDir != null && indexFakeData != null) {
			logger.info("indexing fake vt data");
			
			fakeVirusTotalDataIndexing(parquetInputDir, vtIndexName);;

		}else if (parquetInputDir != null && bloomOutput != null) {
			logger.info("Generating bloom filter");

			generateBloomFilter(parquetInputDir, bloomOutput, statistics);

		} else if(parquetInputDir != null && parquetOutputDir != null) {
			logger.info("deduplicating raw vt data *******************************");

			
			deduplicateVtHashes(parquetInputDir, parquetOutputDir);
			
			
		}else if (bloomFilterInputPath != null && parquetInputDir != null) {
			logger.info("Testing bloomFilter");
		
			bloomFilterTest(bloomFilterInputPath, parquetInputDir);
			
		}else if (bloomFilterInputPath != null && bloomOutput != null) {
			logger.info("merging bloomFilter with fake data");
		
			mergeBloomFilters(bloomFilterInputPath, fakeBloomFilterInputPath, bloomOutput);
			
		}else if(matchBloomFilter!=null && indexName!=null && vtIndexName!=null) {
			
			matchBloomFilterTest(indexName,bloomFilterInputPath,sha256FieldName,vtIndexName);
			
		}else {
			logger.info("Noop selected");

		}

	}

	public static void bloomFilterTest(String bloomFilterInputPath, String dataInputPath) {
		long start = System.currentTimeMillis();
		
		SparkConf conf = new SparkConf().setAppName("VT hash test").setMaster("local[*]");
		JavaSparkContext jsc = new JavaSparkContext(conf);
		SparkSession spark = SparkSession.builder().sparkContext(jsc.sc())

				.getOrCreate();

		try {
			/*
			 * Broadcast<BloomFilter> filter = jsc .broadcast(BloomFilter.readFrom(new
			 * FileInputStream(new File(bloomFilterInputPath)))); Dataset<Row> testData =
			 * spark.read().parquet(dataInputPath + "/*").select(col("sha256"));
			 * 
			 * testData.show(); LongAccumulator positiveCounter =
			 * jsc.sc().longAccumulator();
			 * 
			 * BloomFilterFunction filterFunction = new BloomFilterFunction(filter,
			 * shaFieldName,positiveCounter);
			 * testData.javaRDD().mapPartitions(filterFunction); JavaRDD<Map<String,Object>>
			 * results = testData.javaRDD().mapPartitions(filterFunction); long
			 * positivesNumber = results.count();
			 * System.out.println(positiveCounter.value().toString() + " positives");
			 */

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			spark.close();
		}
		double duration = (System.currentTimeMillis() - start) / 1000;
		System.out.println("bloom filter test duration(s): " + duration);
	}

	

	private static void deduplicateVtHashes(String baseInputDir,String outputDir) {
		logger.info("Removing eventual VT hashes triples duplicates");
		long start = System.currentTimeMillis();
		SparkConf conf = new SparkConf().setAppName("VT hashes duplic&te removal").setMaster("local[*]");
		JavaSparkContext jsc = new JavaSparkContext(conf);

		SparkSession spark = SparkSession.builder().sparkContext(jsc.sc()).getOrCreate();
		
		try {
			Path baseDirectory = Paths.get(new URI("file://"+baseInputDir));
			List<String> subpaths = Files.walk(baseDirectory,1).filter(Files::isDirectory).map(path->path.toAbsolutePath().toString()).collect(Collectors.toList());
			subpaths.remove(baseDirectory.toAbsolutePath().toString());
			
			  Dataset<Row> data = spark.read().parquet(subpaths.toArray(new
			  String[subpaths.size()])).filter(col("av_positives").gt(4)).select(col("md5")
			  , col("sha1"), col("sha256"),col("av_positives"), col("av_tested"),col("fname"),
			  col("record_date")).withColumn("hashTriple",
			  concat_ws("/",col("md5"),col("sha1"),col("sha256")));
			  LongAccumulator duplicatesCounter = jsc.sc().longAccumulator();
			  
			  JavaRDD<Row> duplicates =data.javaRDD().keyBy(row->(String)row.getAs("hashTriple")).groupByKey().map(
			  new DeduplicateFunction(duplicatesCounter));
			  
			  //logger.info("Found {} hashes triple duplicates",duplicateHashesTriples);
			  spark.sqlContext().createDataFrame(duplicates,
			  data.schema()).drop(col("hashTriple")).write().parquet(outputDir);
			  
			  logger.info("{} duplicates found",duplicatesCounter.value());
		 
			
		} catch (Exception e) {
			logger.error("failed to deduplicate VT hashes:",e);
		}finally {
			spark.close();
		}
		
	}
	
	
	
	public static void generateBloomFilter(String inputDir, String bloomFilterOutputPath,
			boolean withStatistics) {

		long start = System.currentTimeMillis();
		SparkSession spark = SparkSession.builder().appName("VT hash filtering").master("local[*]").getOrCreate();
		try {

			Dataset<Row> vts = spark.read().parquet(inputDir + "/*");

			vts.show(10, false);
			long numItems = vts.count();
			

			// positives.write().save(outputDir);
			BloomFilter bf = vts.stat().bloomFilter(col("sha256"), numItems, 0.005);
			bf.writeTo(new FileOutputStream(new File(bloomFilterOutputPath)));
			
			double duration = (System.currentTimeMillis() - start) / 1000;
			System.out.println("duration(s): " + duration);

			
				System.out.println(numItems + " Virus Total hashes ");
			

		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			spark.close();
		}

	}

	public static void elasticReadTest() {

		SparkConf conf = new SparkConf().setAppName("readtest").setMaster("local[*]");

		conf.set("es.nodes", "gui01");

		JavaSparkContext jsc = new JavaSparkContext(conf);

		String indexName = "log-sas-666-suricata-event";

		/*
		 * JavaPairRDD<String, Map<String, Object>> esRDD =JavaEsSpark.esRDD(jsc,
		 * indexName);
		 * 
		 * List<Tuple2<String, Map<String,Object>>> results = esRDD.take(10);
		 * ,positivesNumber,duration
		 * long docsNumber = results.size();
		 * 
		 * System.out.println(docsNumber +" documents found in "+indexName);
		 */

		jsc.stop();

	}
	
	public static void virusTotalIndexing(String inputDir,String indexName) {

		SparkConf conf = new SparkConf().setAppName("VirusTotalIndexing").setMaster("local[*]");

		conf.set("es.nodes", "sim02");

		JavaSparkContext jsc = new JavaSparkContext(conf);

		
		long start = System.currentTimeMillis();
		SparkSession spark = SparkSession.builder().sparkContext(jsc.sc()).getOrCreate();
		try {
		

			Dataset<Row> vts = spark.read().option("mergeSchema", "true").parquet(inputDir + "/*");
			
			JavaRDD<Map<String,Object>> docs = vts.javaRDD().map(new VTIndexingFunction());

			JavaEsSpark.saveToEs(docs, indexName);
			
			double duration = (System.currentTimeMillis() - start) / 1000;
			
			System.out.println("duration(s): " + duration);


		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			
			spark.close();
		
		}

	}
	
	public static void fakeVirusTotalDataIndexing(String inputPath,String indexName) {

		SparkConf conf = new SparkConf().setAppName("fakeVirusTotalIndexing").setMaster("local[*]");

		conf.set("es.nodes", "sim02");

		JavaSparkContext jsc = new JavaSparkContext(conf);

		
		long start = System.currentTimeMillis();
		SparkSession spark = SparkSession.builder().sparkContext(jsc.sc()).getOrCreate();
		try {
			
		

			Dataset<Row> vts = spark.read().option("header", true).csv(inputPath);
			
			vts.show(10,false);
			
			JavaRDD<Map<String,Object>> docs = vts.javaRDD().map(new FakeVTIndexingFunction());
			
			JavaEsSpark.saveToEs(docs, indexName);
			
			double duration = (System.currentTimeMillis() - start) / 1000;
			
			System.out.println("duration(s): " + duration);


		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			
			spark.close();
		
		}

	}
	
	public static void fakeEventsWithHashIndexing(String inputDir) {

		SparkConf conf = new SparkConf().setAppName("VirusTotalIndexing").setMaster("local[*]");

		conf.set("es.nodes", "karabash.bigfoot.local");

		JavaSparkContext jsc = new JavaSparkContext(conf);

		
		long start = System.currentTimeMillis();
		SparkSession spark = SparkSession.builder().sparkContext(jsc.sc()).getOrCreate();
		try {

			Dataset<Row> vts = spark.read().option("mergeSchema", "true").parquet(inputDir + "/*");
			
			JavaRDD<Map<String,Object>> docs = vts.javaRDD().map(new EventIndexingFunction());

			JavaEsSpark.saveToEs(docs, "fake-events-with-hashes");
			
			double duration = (System.currentTimeMillis() - start) / 1000;
			
			System.out.println("duration(s): " + duration);


		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			
			spark.close();
		
		}

	}
	
	public static void mergeBloomFilters(String bloomFilterInputPath,String fakeDataInputPath,String bloomFilterOutputPath) {

		logger.info("Generating fake bloom filter");

		SparkConf conf = new SparkConf().setAppName("merge bloom filter with fake data").setMaster("local[2]");
	

		JavaSparkContext jsc = null;
		
		try {
			jsc = new JavaSparkContext(conf);
			SparkSession spark = SparkSession.builder().sparkContext(jsc.sc()).getOrCreate();
			
			BloomFilter realFilter = BloomFilter.readFrom(new FileInputStream(new File(bloomFilterInputPath)));
			//Broadcast<BloomFilter> filter = jsc.broadcast();

			Dataset<Row> fakeData = spark.read().option("header", true).csv(fakeDataInputPath).select(col("sha256"));


			long numItems = fakeData.count();
			
			
			fakeData.collectAsList().forEach(row -> {
			
				String sha256 = (String)row.getAs("sha256");
				realFilter.put(sha256);
			});

			
			realFilter.writeTo(new FileOutputStream(new File(bloomFilterOutputPath)));
		
			  
			  logger.info("fake filter generated");
			 
		} catch (Exception e) {
			
			logger.error("Failed to build fake bloom filter",e);
			
		}finally {
			

			jsc.stop();
	
			
		}
		
	}
	
	
	public static void matchBloomFilterTest(String eventsIndexName,String bloomFilterInputPath,String sha256FieldName,String virusTotalIndexName) {

		SparkConf conf = new SparkConf().setAppName("matchtest").setMaster("local[*]");

		conf.set("es.nodes", "sim02");
		
		
		
		conf.set("es.query", "{\n"
				+ "  \"query\": {\n"
				+ "   \"bool\":{\n"
				+ "      \"filter\": [\n"
				+ "        { \"range\": { \"@timestamp\": { \"gte\": \"now-50h\" }}},\n"
				+ "        {\"exists\": {\n"
				+ "          \"field\": \"suricata_sha256\"\n"
				+ "        }}\n"
				+ "     ]\n"
				+ "  }\n"
				+ "  },\n"
				+ "  \"fields\": [\"suricata_sha256\"],\n"
				+ "  \"_source\": false\n"
				+ "}");

		JavaSparkContext jsc = null;
		
		try {
			jsc = new JavaSparkContext(conf);

			BloomFilter f = BloomFilter.readFrom(new FileInputStream(new File(bloomFilterInputPath)));
			
			Broadcast<BloomFilter> filter = jsc.broadcast(f);

			logger.info("bloom filter loaded");
			
			  JavaPairRDD<String, Map<String, Object>> esRDD =JavaEsSpark.esRDD(jsc,eventsIndexName);
			  
			  long events = esRDD.count();
			  
			  logger.info("Loading {}  events with hashes",events);
			  
			  long start = System.currentTimeMillis();
			  
			  BloomFilterFunction2 filterFunction = new BloomFilterFunction2();
			  
			  filterFunction.setShaFieldName(sha256FieldName);
			  
			  filterFunction.setFilter(filter);
			  
			  JavaPairRDD<String,Map<String,Object>> positiveCandidates = esRDD.filter(filterFunction);
			  
			  JavaRDD<Map<String,Object>> truePositives = null;//positiveCandidates.mapPartitions(new CheckIocPositivesFunction(virusTotalIndexName, sha256FieldName,eventsIndexName));
			  
			  //long candidatesNumber = positiveCandidates.count();
			  
			 // long positivesNumber = truePositives.count();
			  
				JavaEsSpark.saveToEs(truePositives, "true-vt-positives");

			  
			  double duration = (System.currentTimeMillis()-start)/1000L;
			  
			 // logger.info("found {} total true positives  in {} seconds",positivesNumber,duration);
			 
		} catch (Exception e) {
			
			logger.error("Failed to match bloom filter",e);
			
		}finally {
			

			jsc.stop();
	
			
		}
		
	}

	public static class BloomFilterFunction2 implements Function<Tuple2<String, Map<String, Object>>, Boolean>,Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private static transient Logger logger = LoggerFactory.getLogger(BloomFilterFunction.class);

		public Broadcast<BloomFilter> bloomFilter;
		//private LongAccumulator positiveCounter;
		public boolean generateSha256;
		public String shaFieldName;

		public BloomFilterFunction2() {
			
		}

		public void setShaFieldName(String shaFieldName) {
			this.shaFieldName = shaFieldName;

		}
		
		public void setFilter(Broadcast<BloomFilter> aFilter) {
			this.bloomFilter = aFilter;
		}
		
		

		public void setGenerateSha256(boolean generateSha256) {
			this.generateSha256 = generateSha256;
		}

		public Boolean call(Tuple2<String, Map<String, Object>> tuple) throws Exception {

			boolean isCandidate = false;

			Map<String, Object> candidateDocWithSha256 = tuple._2;

			String sha256 = getSha256(shaFieldName, candidateDocWithSha256);

			if (sha256 != null) {

				boolean isPositive = bloomFilter.value().mightContainString(sha256);

				if (isPositive) {

					isCandidate = isPositive;
				}
			}

			return isCandidate;
		}

		private String getSha256(String sha256FieldName, Map<String, Object> candidate) {
			String sha256 = null;
			Object value = null;
			if (sha256FieldName != null) {
				/*
				 * String[] objFields = sha256FieldName.split("\\."); for (int i = 0; i <
				 * objFields.length; i++) { String fieldKey = objFields[i];
				 * if(i<objFields.length-1) { if(value==null) { value = candidate.get(fieldKey);
				 * }else { value = ((Map<String,Object>)value).get(fieldKey); } }else { sha256 =
				 * (String)((Map<String,Object>)value).get(fieldKey); } }
				 */
				sha256 = (String) candidate.get(sha256FieldName);
				if (sha256 == null) {
					Optional<Object> optSha256 = getOptionalValue(sha256FieldName, candidate, false);
					if (optSha256.isPresent()) {
						sha256 = (String) optSha256.get();
					}
				}
			}

			return sha256;
		}

		public Optional<Object> getOptionalValue(String fieldName, Map<String, Object> entity, boolean... withRemoval) {

			boolean remove = (withRemoval != null && withRemoval.length == 1) ? withRemoval[0] : false;

			Optional<Object> value = Optional.empty();

			String[] splittedFieldName = fieldName.split("\\.");

			for (int i = 0; i < splittedFieldName.length; i++) {

				String key = splittedFieldName[i];

				if (entity.containsKey(key)) {

					value = Optional.ofNullable(entity.get(key));

					if (i < splittedFieldName.length - 1) {
						if (!value.isPresent()) {
							break;
						}
						entity = (Map<String, Object>) value.get();
					} else if (remove) {
						entity.remove(key);
					}

				} else {

					value = Optional.empty();
					break;
				}

			}

			return value;

		}

		private Object getFieldValue(String fieldKey, Map<String, Object> container) {
			Object fieldValue = null;

			return fieldValue;
		}

	}

}
