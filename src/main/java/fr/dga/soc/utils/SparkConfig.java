/**
 * 
 */
package fr.dga.soc.utils;

/**
 * @author pcaparroy
 *
 */
public class SparkConfig {

	private String masterUrl;
	private String driverMemory;
	private String executorMemory;
	
	private String java_home;
	
	/**
	 * 
	 */
	public SparkConfig() {
		
	}



	public String getMasterUrl() {
		return masterUrl;
	}



	public void setMasterUrl(String masterUrl) {
		this.masterUrl = masterUrl;
	}



	public String getDriverMemory() {
		return driverMemory;
	}



	public void setDriverMemory(String driverMemory) {
		this.driverMemory = driverMemory;
	}



	public String getExecutorMemory() {
		return executorMemory;
	}



	public void setExecutorMemory(String executorMemory) {
		this.executorMemory = executorMemory;
	}



	public String getJava_home() {
		return java_home;
	}



	public void setJava_home(String java_home) {
		this.java_home = java_home;
	}
	
	

}
