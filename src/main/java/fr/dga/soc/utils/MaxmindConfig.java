/**
 * 
 */
package fr.dga.soc.utils;

/**
 * @author pca
 *
 */
public class MaxmindConfig {
	
	private String databasePath;

	public MaxmindConfig() {
		super();
	}

	public String getDatabasePath() {
		return databasePath;
	}

	public void setDatabasePath(String databasePath) {
		this.databasePath = databasePath;
	}

}
