/**
 * 
 */
package fr.dga.soc.utils;

/**
 * @author pcaparroy
 *
 */
public class MispConfig {

	private String mispAdminUserId;
	private String url;
	private String mispSShUser;
	private String mispSShPassword;
	private int mispSShPort;
	private String authKey;
	private String downloadPath;
	private boolean feedsImport;
	
	/**
	 * 
	 */
	public MispConfig() {

	}

	
	
	public String getDownloadPath() {
		return downloadPath;
	}
	



	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}



	public String getMispAdminUserId() {
		return mispAdminUserId;
	}

	public void setMispAdminUserId(String mispAdminUserId) {
		this.mispAdminUserId = mispAdminUserId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMispSShUser() {
		return mispSShUser;
	}

	public void setMispSShUser(String mispSShUser) {
		this.mispSShUser = mispSShUser;
	}

	public String getMispSShPassword() {
		return mispSShPassword;
	}

	public void setMispSShPassword(String mispSShPassword) {
		this.mispSShPassword = mispSShPassword;
	}

	

	public int getMispSShPort() {
		return mispSShPort;
	}

	public void setMispSShPort(int mispSShPort) {
		this.mispSShPort = mispSShPort;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	public String getAuthKey() {
		return authKey;
	}



	public boolean isFeedsImport() {
		return feedsImport;
	}



	public void setFeedsImport(boolean feedsImport) {
		this.feedsImport = feedsImport;
	}
	
	

}
