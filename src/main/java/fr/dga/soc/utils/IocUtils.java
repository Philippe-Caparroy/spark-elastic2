package fr.dga.soc.utils;

public class IocUtils {

	public IocUtils() {
		
		
	}
	
	public static boolean needsSha256Conversion(String type) {
		boolean needsSha256=false;
		
		switch (type) {
		case "url":
			needsSha256=true;
			break;
		case "ip-dst|port":
			needsSha256=true;
			break;
		case "domain":
			needsSha256=true;
			break;
		case "ip-dst":
			needsSha256=true;
			break;
		case "ip-src":
			needsSha256=true;
			break;
			
		case "filename":
			needsSha256=true;
			break;
		default:
			needsSha256=false;
			break;
		}
		
		return needsSha256;
	}

}
