/**
 * 
 */
package fr.dga.soc.utils;

import java.util.Map;

/**
 * @author pcaparroy
 *
 */
public class IocConfig {

	private boolean generateSha256;
	
	private String hashSourceFieldName;
	
	private String indexName;
	
	private String truePositivesDetectionWindow;
	
	private String rawDataPath;
	
	private String deduplicatedDataPath;

	private String bloomFilterPath;
	
	private String encoding;
	
	private Map<String,LogConfig> targets;
	
	private boolean deactivated;
	
	/**
	 * 
	 */
	public IocConfig() {

	}

	public boolean isGenerateSha256() {
		return generateSha256;
	}

	public void setGenerateSha256(boolean generateSha256) {
		this.generateSha256 = generateSha256;
	}

	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public String getTruePositivesDetectionWindow() {
		return truePositivesDetectionWindow;
	}

	public void setTruePositivesDetectionWindow(String truePositivesDetectionWindow) {
		this.truePositivesDetectionWindow = truePositivesDetectionWindow;
	}

	

	public String getRawDataPath() {
		return rawDataPath;
	}

	public void setRawDataPath(String rawDataPath) {
		this.rawDataPath = rawDataPath;
	}
	
	public String getDeduplicatedDataPath() {
		return deduplicatedDataPath;
	}

	public void setDeduplicatedDataPath(String deduplicatedDataPath) {
		this.deduplicatedDataPath = deduplicatedDataPath;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public Map<String, LogConfig> getTargets() {
		return targets;
	}

	public void setTargets(Map<String, LogConfig> targets) {
		this.targets = targets;
	}

	public String getBloomFilterPath() {
		return bloomFilterPath;
	}

	public void setBloomFilterPath(String bloomFilterPath) {
		this.bloomFilterPath = bloomFilterPath;
	}

	public String getHashSourceFieldName() {
		return hashSourceFieldName;
	}

	public void setHashSourceFieldName(String hashSourceFieldName) {
		this.hashSourceFieldName = hashSourceFieldName;
	}

	public boolean isDeactivated() {
		return deactivated;
	}

	public void setDeactivated(boolean deactivated) {
		this.deactivated = deactivated;
	}

	
	
	

}
