/**
 * 
 */
package fr.dga.soc.utils;

/**
 * @author pcaparroy
 *
 */
public class ElasticsearchConfig {

	private String nodes;
	
	private long port;
	
	private boolean feedsIndexing;
	
	private String kibanaUrl;
	
	private String kibanaVersion = "7.15.0";
	
	private String kibanaSavedObjectsPath;
	
	private String templatesPath;
	
	private String ilmPoliciesPath;
	
	/**
	 * 
	 */
	public ElasticsearchConfig() {

	}
	
	
	public String getNodes() {
		return nodes;
	}
	public void setNodes(String elasticsearchNodes) {
		this.nodes = elasticsearchNodes;
	}
	public long getPort() {
		return port;
	}
	public void setPort(long elasticsearchPort) {
		this.port = elasticsearchPort;
	}


	

	public String getKibanaUrl() {
		return kibanaUrl;
	}


	public void setKibanaUrl(String kibanaUrl) {
		this.kibanaUrl = kibanaUrl;
	}


	public String getKibanaVersion() {
		return kibanaVersion;
	}


	public void setKibanaVersion(String kibanaVersion) {
		this.kibanaVersion = kibanaVersion;
	}


	public String getKibanaSavedObjectsPath() {
		return kibanaSavedObjectsPath;
	}

	public void setKibanaSavedObjectsPath(String kibanaSavedObjectsPath) {
		this.kibanaSavedObjectsPath = kibanaSavedObjectsPath;
	}


	public String getTemplatesPath() {
		return templatesPath;
	}


	public void setTemplatesPath(String templatesPath) {
		this.templatesPath = templatesPath;
	}

	public String getIlmPoliciesPath() {
		return ilmPoliciesPath;
	}


	public void setIlmPoliciesPath(String ilmPoliciesPath) {
		this.ilmPoliciesPath = ilmPoliciesPath;
	}


	public boolean isFeedsIndexing() {
		return feedsIndexing;
	}


	public void setFeedsIndexing(boolean feedsIndexing) {
		this.feedsIndexing = feedsIndexing;
	}
	

}
