/**
 * 
 */
package fr.dga.soc.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.maxmind.db.CHMCache;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.AnonymousIpResponse;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.model.ConnectionTypeResponse.ConnectionType;
import com.maxmind.geoip2.record.Location;
import com.maxmind.geoip2.record.Traits;



/**
 * @author pca
 *
 */
public class MaxmindGeolocProcessor {
	
    private DatabaseReader citiesDatabase;

	
	public MaxmindGeolocProcessor(String databasePath) throws Exception{
		if(databasePath!=null) {
			InputStream in = new FileInputStream(new File(databasePath));
			
			citiesDatabase =  new DatabaseReader.Builder(in).withCache(new CHMCache()).build();
		}
	}
	
	
	
	
	
	public void generateCityEnrichment(InetAddress address, Map<String, Object> enrichment)
            throws GeoIp2Exception, IOException {
        if (citiesDatabase != null) {
            Optional<CityResponse> cityResponse = citiesDatabase.tryCity(address);
            if (cityResponse.isPresent()) {

                CityResponse response = cityResponse.get();

                String continent = response.getContinent().getName();
                enrich(MaxmindVocabulary.CONTINENT, continent, enrichment);
                
                
                
                String countryName = response.getCountry().getName();
                enrich(MaxmindVocabulary.COUNTRY, countryName,enrichment);
               
                String countryCode = response.getCountry().getIsoCode();
                enrich(MaxmindVocabulary.COUNTRY_CODE, countryCode,enrichment);
               
                Integer countryConfidence = response.getCountry().getConfidence();

                Integer countryGeonamesId = response.getCountry().getGeoNameId();

                enrich(MaxmindVocabulary.CITY,response.getCity().getName(),enrichment);

                Integer cityConfidence = response.getCity().getConfidence();

                Integer cityGeonamesId = response.getCity().getGeoNameId();

                
                generateLocationEnrichment(response.getLocation(), enrichment);
                
                Traits traits = response.getTraits();
                
                enrich(MaxmindVocabulary.AS_NUMBER,traits.getAutonomousSystemNumber(),enrichment);
                enrich(MaxmindVocabulary.AS_ORGANISATION,traits.getOrganization(),enrichment);
                enrich(MaxmindVocabulary.CONNECTION_TYPE,traits.getConnectionType(),enrichment);
                enrich(MaxmindVocabulary.DOMAIN,traits.getDomain(),enrichment);
                enrich(MaxmindVocabulary.ISP_NAME,traits.getIsp(),enrichment);
                enrich(MaxmindVocabulary.ISP_ORGANISATION,traits.getOrganization(),enrichment);
                enrich(MaxmindVocabulary.STATIC_IP_SCORE,traits.getStaticIpScore(),enrichment);

              
                enrich(MaxmindVocabulary.IS_ANONYMOUS,traits.isAnonymous(),enrichment);
                enrich(MaxmindVocabulary.IS_ANONYMOUS_VPN,traits.isAnonymousVpn(),enrichment);
                enrich(MaxmindVocabulary.IS_ANONYMOUS_PROXY,traits.isAnonymousProxy(),enrichment);
                enrich(MaxmindVocabulary.IS_HOSTING_PROVIDER,traits.isHostingProvider(),enrichment);

                ConnectionType conntype = traits.getConnectionType();
                if(conntype!=null) {
                	
                	 enrich(MaxmindVocabulary.CONNECTION_TYPE,conntype.toString(),enrichment);
                	
                }
               


            }
            
           // generateAnonymousIpEnrichment(address, enrichment);
        }
    }
	
	public void generateAnonymousIpEnrichment(InetAddress address, Map<String, Object> enrichment)
            throws GeoIp2Exception, IOException {
        if (citiesDatabase != null) {
            Optional<AnonymousIpResponse> anonymousIpResponse = citiesDatabase.tryAnonymousIp(address);

            if (anonymousIpResponse.isPresent()) {

                AnonymousIpResponse response = anonymousIpResponse.get();

                enrichment.put(MaxmindVocabulary.IS_ANONYMOUS, response.isAnonymous());

                enrichment.put(MaxmindVocabulary.IS_ANONYMOUS_VPN, response.isAnonymousVpn());

                enrichment.put(MaxmindVocabulary.IS_HOSTING_PROVIDER, response.isHostingProvider());

                enrichment.put(MaxmindVocabulary.IS_PUBLIC_PROXY, response.isPublicProxy());

                enrichment.put(MaxmindVocabulary.IS_RESIDENTIAL_PROXY, response.isResidentialProxy());

                enrichment.put(MaxmindVocabulary.IS_TOR_EXIT_NODE, response.isTorExitNode());

            }
        }

    }
	
	private void enrich(String field,Object value,Map<String,Object> collector) {
		if(value!=null) {
			collector.put(field, value);
		}
	}

    public void generateLocationEnrichment(Location location, Map<String, Object> enrichment) {
        if (location != null) {

            Integer accuracyRadius = location.getAccuracyRadius();
            enrichment.put(MaxmindVocabulary.ACCURACY_RADIUS, accuracyRadius);
            Double lat = location.getLatitude();
            

            Double lon = location.getLongitude();
            Map<String,Object> loc = new HashMap<String, Object>();
            loc.put("lat", lat);
            loc.put("lon", lon);

            enrichment.put("location",loc);

        }
    }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		

	}

}
