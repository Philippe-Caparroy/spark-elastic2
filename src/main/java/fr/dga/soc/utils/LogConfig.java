/**
 * 
 */
package fr.dga.soc.utils;

/**
 * @author pcaparroy
 *
 */
public class LogConfig {

	
	private String indexPattern;
	
	private String sha256FieldAlias;
	/**
	 * 
	 */
	public LogConfig() {

	}
	public String getIndexPattern() {
		return indexPattern;
	}
	public void setIndexPattern(String indexPattern) {
		this.indexPattern = indexPattern;
	}
	public String getSha256FieldAlias() {
		return sha256FieldAlias;
	}
	public void setSha256FieldAlias(String sha256FieldAlias) {
		this.sha256FieldAlias = sha256FieldAlias;
	}

	

}
