/**
 * 
 */
package fr.dga.soc.utils;

/**
 * @author pca
 *
 */
public interface MaxmindVocabulary {


		static final String IS_ANONYMOUS_VPN="isAnonymousVpn";
		static final String IS_ANONYMOUS_PROXY="isAnonymousProxy";

		static final String IS_ANONYMOUS="isAnonymous";
		static final String IS_HOSTING_PROVIDER="isHostingProvider";
		static final String IS_PUBLIC_PROXY="isPublicProxy";
		static final String IS_RESIDENTIAL_PROXY="isResidentialProxy";
		static final String IS_TOR_EXIT_NODE="isTorExitNode";
		static final String CONNECTION_TYPE="connection_type";
		static final String AS_NUMBER="as_number";
		static final String AS_ORGANISATION="as_organisation";
		static final String ISP_NAME="isp_name";
		static final String ISP_ORGANISATION="isp_organisation";
		
		static final String STATIC_IP_SCORE="static_ip_score";

		static final String DOMAIN="domain_maxmind";
		static final String LATLON="location.latlon";

		static final String LON="location.lon";
		static final String LAT="location.lat";
		static final String ACCURACY_RADIUS="location_accuracyRadius";
		static final String CITY="city";
		static final String COUNTRY="country";
		static final String COUNTRY_CODE="country_code";
		static final String CONTINENT="continent";

	
}
