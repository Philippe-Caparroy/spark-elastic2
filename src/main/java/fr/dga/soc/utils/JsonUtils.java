/**
 * 
 */
package fr.dga.soc.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import org.apache.curator.shaded.com.google.common.io.ByteSource;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;
//import com.google.common.io.ByteSource;

// TODO: enrich progressively Javadoc
/**
 * The Class JsonUtils.
 *
 * @author pcaparroy
 */
public class JsonUtils {

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(JsonUtils.class);
    
    /**
     * Instantiates a new json utils.
     */
    public JsonUtils() {

    }
    
    public static Optional<JSONObject> loadJsonObject(String filePath) {
    	Optional<JSONObject> obj = Optional.empty();
    	
    	try {
		
        	InputStream input = new FileInputStream(new File(filePath));

        	JSONObject anObj = new JSONObject(new JSONTokener(input));
    		
        	obj = Optional.of(anObj);
        	
		} catch (Exception e) {
			logger.error("Failed to generate json object",e);
		}    	
    	
    	
    	return obj;
    }
    
    
    /**
     * As json string.
     *
     * @param is the is
     * @return the string
     */
	
	  public static String asJsonString(InputStream is) { String js=null;
	  
	  try { ByteSource byteSource = new ByteSource() {
	  
	  @Override public InputStream openStream() throws IOException { return is; }
	  }; js=byteSource.asCharSource(Charsets.UTF_8).read(); } catch (Exception e) {
	  logger.error("Failed to convert Inputstream into json tring",e); }
	  
	  
	  return js; }
	 

}
