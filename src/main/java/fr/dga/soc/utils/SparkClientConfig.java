/**
 * 
 */
package fr.dga.soc.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author pcaparroy
 *
 */
public class SparkClientConfig {

	private ElasticsearchConfig elasticsearch;
	
	private SparkConfig spark;
	
	private Map<String,IocConfig> iocs;
	
	private MispConfig misp;
	
	private MaxmindConfig maxmind;
	
	private String proxyUri;
	
	/**
	 * 
	 */
	public SparkClientConfig() {
		this.elasticsearch = new ElasticsearchConfig();
		this.iocs= new HashMap<>();
		this.spark = new SparkConfig();
		this.misp = new MispConfig();
		this.maxmind = new MaxmindConfig();
	}
	
	public ElasticsearchConfig getElasticsearch() {
		return elasticsearch;
	}

	public void setElasticsearch(ElasticsearchConfig elasticsearch) {
		this.elasticsearch = elasticsearch;
	}

	public Map<String, IocConfig> getIocs() {
		return iocs;
	}

	public void setIocs(Map<String, IocConfig> iocs) {
		this.iocs = iocs;
	}

	public SparkConfig getSpark() {
		return spark;
	}

	public void setSpark(SparkConfig spark) {
		this.spark = spark;
	}

	public MispConfig getMisp() {
		return misp;
	}

	public void setMisp(MispConfig misp) {
		this.misp = misp;
	}

	public MaxmindConfig getMaxmind() {
		return maxmind;
	}

	public void setMaxmind(MaxmindConfig maxmind) {
		this.maxmind = maxmind;
	}

	public String getProxyUri() {
		return proxyUri;
	}

	public void setProxyUri(String proxyUri) {
		this.proxyUri = proxyUri;
	}

	
}
