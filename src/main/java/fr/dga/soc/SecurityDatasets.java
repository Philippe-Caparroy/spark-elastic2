/**
 * 
 */
package fr.dga.soc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructField;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;
import org.elasticsearch.spark.sql.api.java.JavaEsSparkSQL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.dga.soc.spark.SparkParameterTool;
import fr.dga.soc.spark.functions.SDIndexingFunction;

/**
 * @author pcaparroy
 *
 */
public class SecurityDatasets {

	private static Logger logger = LoggerFactory.getLogger(SecurityDatasets.class);

	/**
	 * 
	 */
	public SecurityDatasets() {
		
	}
	
	private static void createIndex(String indexName,String jsonInputFile) {
		
		SparkConf conf = new SparkConf().setAppName("securityDatasetsTest").setMaster("local[2]");

		conf.set("es.nodes", "sim02,sim01");

		JavaSparkContext jsc = new JavaSparkContext(conf);

		
		long start = System.currentTimeMillis();
		SparkSession spark = SparkSession.builder().sparkContext(jsc.sc()).getOrCreate();
		try {

			Dataset<Row> sdd = spark.read().option("timestampFormat", "yyyy/MM/dd HH:mm:ss ZZ").format("json").load(jsonInputFile);

			
			sdd.show(2, false);

			List<String> fieldNames = new ArrayList<>();
			
			StructField[] fields = sdd.schema().fields();
			
			for(StructField field:fields) {
				fieldNames.add(field.name());
			}
			
			JavaRDD<Map<String,Object>> docs = sdd.javaRDD().map(new SDIndexingFunction(fieldNames));
			

			JavaEsSpark.saveToEs(docs, indexName);
			
			
			double duration = (System.currentTimeMillis() - start) / 1000;
			System.out.println("duration(s): " + duration);

					

		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			spark.close();
		}
		
		
	}
	
	private static void readIndex(String indexName) {
		
		SparkConf conf = new SparkConf().setAppName("securityDatasetsTest").setMaster("local[2]");

		conf.set("es.nodes", "sim02");
		conf.set("es.read.field.include", "event.code,process.guid");
		conf.set("spark.debug.maxToStringFields","100");
		
		//conf.set("es.read.field.as.array.include","tags");

		JavaSparkContext jsc = new JavaSparkContext(conf);
		
		SQLContext sql = new SQLContext(jsc);
		
		try {
			Dataset<Row> securityEvents = JavaEsSparkSQL.esDF(sql, indexName); 
			
			
			securityEvents.createTempView("events");
			//securityEvents.createTempView("events2");
			/*
			 * Dataset<Row> sqlDF = sql.
			 * sql("SELECT `@timestamp`, Hostname, SubjectUserName, TargetUserName, NewProcessName, CommandLine "
			 * + "FROM events" + " WHERE LOWER(Channel) = \"security\"" +
			 * "    AND EventID = 4688" +
			 * "    AND lower(ParentProcessName) LIKE \"%wmiprvse.exe\"" +
			 * "    AND NOT TargetLogonId = \"0x3e7\"");
			 */
			
			Dataset<Row> sqlDF = sql.sql("SELECT e1.guid, e1.code,e2.code FROM (select e.process.guid, e.event.code FROM events e WHERE e.event.code = 1) e1 INNER JOIN (select e.process.guid, e.event.code FROM events e WHERE e.event.code = 3) e2 ON e1.guid == e2.guid");
			
			sqlDF.show(200,false);
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			jsc.stop();
		}

		
	}
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		SparkParameterTool sparkParameters = SparkParameterTool.fromArgs(args);

//		String jsonInputFile = sparkParameters.get("jsonInputFile");

		//createIndex(indexName, jsonInputFile);
		
		String indexName = sparkParameters.get("indexName");

		
		
		readIndex(indexName);
		
	}

}
