/**
 * 
 */
package fr.dga.soc.elasticsearch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.http.HttpHost;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.support.replication.ReplicationResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.ElasticsearchClient;
import org.elasticsearch.client.Node;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.index.query.ExistsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.CardinalityAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.ParsedCardinality;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.dga.soc.elasticsearch.scripts.BashScript;
import fr.dga.soc.elasticsearch.scripts.UpdateComponentTemplateScript;
import fr.dga.soc.elasticsearch.scripts.UpdateIlmPolicyScript;
import fr.dga.soc.elasticsearch.scripts.UpdateTemplateScript;
import fr.dga.soc.utils.ElasticsearchConfig;
import fr.dga.soc.utils.IocConfig;
import fr.dga.soc.utils.LogConfig;

/**
 * @author pcaparroy
 *
 */
public class ElasticsearchManager {

	private List<String> elasticsearchHosts;
	private Logger logger = LoggerFactory.getLogger(ElasticsearchManager.class);
	private String componentTemplatesPath;

	private String ilmPoliciesPath;

	private String templatesPath;
	private String elasticsearchUrl;
	/**
	 * 
	 */
	public ElasticsearchManager(ElasticsearchConfig config) throws Exception{
		this.elasticsearchHosts = new ArrayList<>();
		String[] esNodes = config.getNodes().split(",");
		for(String node:esNodes) {
			this.elasticsearchHosts.add("http://"+node+":"+config.getPort());
		}
		this.elasticsearchUrl="http://"+config.getNodes().split(",")[0]+":"+config.getPort();

		this.templatesPath=config.getTemplatesPath();
		this.componentTemplatesPath=templatesPath+"/components";
		this.ilmPoliciesPath=config.getIlmPoliciesPath();

	}
	
	public long countIocs(String iocType) {
		
		long iocsNumber=0L;
		
		RestHighLevelClient client = null;
		
		try {
			
			client = getElasticClient();
			
			SearchRequest search = new SearchRequest("iocs-abuse.ch");
			SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
			
			if(iocType.equals("ip-dst-port")) {
				iocType = "ip-dst|port";
			}
			CardinalityAggregationBuilder aggBuildder = AggregationBuilders.cardinality("count_distinct").field(iocType);//filter("type_exists", QueryBuilders.existsQuery(iocType));
			sourceBuilder.aggregation(aggBuildder);
			sourceBuilder.size(0);
			search.source(sourceBuilder);
			
			SearchResponse response = client.search(search, RequestOptions.DEFAULT);
			
			ParsedCardinality agg = response.getAggregations().get("count_distinct");
			
			iocsNumber = agg.getValue();
			
		} catch (Exception e) {
			logger.error("Failed to count iocs of type {}",iocType,e);
		}finally {
			closeClient(client);
		}
		
		
		return iocsNumber;
	}
	
	
	
	
	public void generateIocTests(String iocType,long totalNumber,String targetLogIndexName,String logFieldName) {
		
		RestHighLevelClient client = null;
		
		List<DocWriteRequest<?>> bloomIocs = new ArrayList<>();
		
		List<DocWriteRequest<?>> logsIocs = new ArrayList<>();

		
		
		try {
			
			Set<String> uniqueIocs = new HashSet<>();
			
			long sampleSize = Math.floorDiv(totalNumber, 2);
			
			client = getElasticClient();
			
			final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));
			
			SearchRequest search = new SearchRequest("iocs-abuse.ch");
			
			search.scroll(scroll);
			
			SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
			
			sourceBuilder.size(1000);
			
			String iocFieldName = iocType;
			
			String[] logFields = null;
			
			if(iocFieldName.equals("ip-dst-port")) {
			
				iocFieldName = "ip-dst|port";
				
				logFields = logFieldName.split("\\|");
			
			}
			
			ExistsQueryBuilder filter = QueryBuilders.existsQuery(iocFieldName);
			
			sourceBuilder.query(filter);
			
			search.source(sourceBuilder);
			
			SearchResponse response = client.search(search, RequestOptions.DEFAULT);
			
			String scrollId = response.getScrollId();
			
			SearchHit[] searchHits = response.getHits().getHits();
			
			while (searchHits != null && searchHits.length > 0) { 
			    
				for(SearchHit hit:searchHits) {
					Map<String,Object> anIoc = hit.getSourceAsMap();
					if(uniqueIocs.add((String)anIoc.get(iocFieldName))) {
						if(bloomIocs.size()<sampleSize) {
														
							bloomIocs.add(asUpdateRequest("iocs-abuse.ch-"+iocType+"-test", anIoc, hit.getId()));
							Map<String,Object> logEvt = new HashMap<>();
							if(iocType.equals("ip-dst-port")) {
								String[] values = ((String)anIoc.get(iocFieldName)).split("\\|");
								for(int i=0;i<logFields.length;i++) {
									logEvt.put(logFields[i], values[i]);
								}
							}else {
								logEvt.put(logFieldName, anIoc.get(iocFieldName));
							}
							logEvt.put("@timestamp", System.currentTimeMillis());
							logsIocs.add(asIndexRequest(targetLogIndexName, logEvt, hit.getId()));
						}else{
							
							Map<String,Object> logEvt = new HashMap<>();
							if(iocType.equals("ip-dst-port")) {
								String[] values = ((String)anIoc.get(iocFieldName)).split("\\|");
								for(int i=0;i<logFields.length;i++) {
									logEvt.put(logFields[i], values[i]);
								}
							}else {
								logEvt.put(logFieldName, anIoc.get(iocFieldName));
							}
							logEvt.put("@timestamp", System.currentTimeMillis());
							logsIocs.add(asIndexRequest(targetLogIndexName, logEvt, hit.getId()));
	
						}
					}
					//consummedIocs+=1;
				}
				
			    SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId); 
			    scrollRequest.scroll(scroll);
			    response = client.scroll(scrollRequest, RequestOptions.DEFAULT);
			    scrollId = response.getScrollId();
			    searchHits = response.getHits().getHits();
			}
			
			ClearScrollRequest clearScrollRequest = new ClearScrollRequest(); 
			clearScrollRequest.addScrollId(scrollId);
			ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
			boolean succeeded = clearScrollResponse.isSucceeded();
			
		} catch (Exception e) {
			logger.error("Failed to count iocs of iocType {}",iocType,e);
		}finally {
			closeClient(client);
		}
		
		
		if(bloomIocs.size()>0) {
			bulkIndex(bloomIocs);
		}
		
		if(logsIocs.size()>0) {
			bulkIndex(logsIocs);
		}
	}
	
	
    public boolean bulkIndex(List<DocWriteRequest<?>> indexRequests) {
        
        boolean bulkIndexationSuccess = true;
            
            RestHighLevelClient theElasticClient = null;
            
            BulkProcessor bulkProcessor = null;
            try {
                
                final RestHighLevelClient elasticClient = getElasticClient();
                theElasticClient = elasticClient;
                
                BulkProcessor.Listener bulkListener = new BulkProcessor.Listener() { 
                    @Override
                    public void beforeBulk(long executionId, BulkRequest request) {
                        int numberOfActions = request.numberOfActions(); 
                       logger.info("Executing bulk [{}] with {} requests",executionId, numberOfActions);
                    }

                    @Override
                    public void afterBulk(long executionId, BulkRequest request,
                            BulkResponse response) {
                        if (response.hasFailures()) { 
                            logger.warn("Bulk [{}] executed with failures", executionId);
                            BulkItemResponse[] responseItems = response.getItems();
                            for (int i = 0; i < responseItems.length; i++) {
                                BulkItemResponse bulkItemResponse = responseItems[i];
                                if(bulkItemResponse.isFailed()){
                                String failureMessage = bulkItemResponse.getFailureMessage();
                                logger.warn("Indexing failed due to {}",failureMessage);
                                    // failedRequestCounter.add(1L);
                                }else{
                                   // deleteRequestCounter.add(1L);
                                }
                            }
                           
                        } else {
                            logger.info("Bulk [{}] completed in {} milliseconds",
                                    executionId, response.getTook().getMillis());
                           //deleteRequestCounter.add(response.getItems().length);
                        }
                    }

                    @Override
                    public void afterBulk(long executionId, BulkRequest request,
                            Throwable failure) {
                        logger.error("Failed to execute bulk", failure);
                       // exceptionsCounter.add(1L);
                    }
                };
                
                BulkProcessor.Builder builder = BulkProcessor.builder(
                        (request, listner) ->
                        elasticClient.bulkAsync(request, RequestOptions.DEFAULT, listner),
                        bulkListener);
                
             //   builder.setBulkActions(bulkConfig.getBulkFlushMaxActions()); 
               // builder.setBulkSize(new ByteSizeValue(bulkConfig.getBulkFlushMaxSize(), ByteSizeUnit.MB)); 
                builder.setConcurrentRequests(0); 
                builder.setFlushInterval(TimeValue.timeValueMillis(5000)); 
                //if(bulkConfig.isBackoffEnabled()){
                //builder.setBackoffPolicy(BackoffPolicy
                  //      .constantBackoff(TimeValue.timeValueMillis(bulkConfig.getBulkFlushBackoffDelay()), bulkConfig.getBulkFlushBackoffRetries()));
                //}
                
                 bulkProcessor = builder.build();
                
                
                for (DocWriteRequest<?> indexRequest:indexRequests) {
                    
                           // indexRequest.timeout(TimeValue.timeValueMinutes(1)); 
                            
                            bulkProcessor.add(indexRequest);
             
                }
                
            } catch (Exception e) {
                bulkIndexationSuccess=false;
              //  collectStackTrace(e);
                logger.error("Failed to perform bulk Indexing",e);
                
               // exceptionsCounter.add(1L);
            }finally{
            
            
                if(bulkProcessor!=null){
                    try {
                        boolean terminated = bulkProcessor.awaitClose(2, TimeUnit.SECONDS);
                        if(terminated){
                            logger.info("Elasticsearch Bulk  processor closed");
                        }else{
                            logger.error("Elasticsearch Bulk  processor is not closed");
                        }
                    } catch (Exception e2) {
                        logger.error("Failed to terminate bulk indexing",e2);
                    }
                    
                }
                
                closeClient(theElasticClient);
            }
           
        
        return bulkIndexationSuccess;
        
    }
    
    public IndexRequest asIndexRequest(String indexName,Map<String,Object> doc,String id){
    	IndexRequest indexRequest = new IndexRequest(indexName.toLowerCase().replaceAll(" ", "-")).id(id).create(true);

    	//if(doc.containsKey("ip-dst")) {
        //	indexRequest = indexRequest.setPipeline("geoip");
//
    	//}
    	
        indexRequest.source(doc);
      return indexRequest;
    }
   
    
    public UpdateRequest asUpdateRequest(String indexName,Map<String,Object> doc,String id){
    	
    	UpdateRequest indexRequest = new UpdateRequest(indexName.toLowerCase().replaceAll(" ", "-"),id);
    	
        indexRequest = indexRequest.doc(doc).docAsUpsert(true);
        
        return indexRequest;
    }
	
	 public boolean indexDocument(String indexName,Map<String,Object> doc,String id){
	        boolean docIndexed = true;
	        RestHighLevelClient elasticClient=null;
	        try {
	            elasticClient = getElasticClient();
	            
	            IndexRequest indexRequest = asIndexRequest(indexName, doc, id);
	            IndexResponse response = elasticClient.index(indexRequest, RequestOptions.DEFAULT);
	            if(response.getResult()==DocWriteResponse.Result.CREATED){
	                logger.debug("document successfully created");
	            }else if(response.getShardInfo().getFailed()>0){
	                
	            	for (ReplicationResponse.ShardInfo.Failure failure :
	                    
	                	response.getShardInfo().getFailures()) {
	                    
	                    logger.warn("Failed to index execution report : {}",failure.reason());
	                    
	                }
	                docIndexed=false;
	            }
	        
	        } catch (Exception e) {
	           // logger.error("Failed to index document ",e);
	            docIndexed=false;
	        }finally {
	            closeClient(elasticClient);
	        }
	        
	        return docIndexed;
	    }
	 
	 private void closeClient(RestHighLevelClient client){
	        if (client != null) {
	            try {
	                client.close();
	            } catch (Exception e2) {
	               // collectStackTrace(e2);
	                logger.error("Failed to close elasticsearch rest client", e2);
	            }

	        }
	    }
	 public RestHighLevelClient getElasticClient() throws Exception {

	        return getElasticClient(getElastisearchHosts(elasticsearchHosts));
	    }
	 
	 public RestHighLevelClient getElasticClient(Node[] elasticNodes) throws Exception {

	        RestClientBuilder clientBuilder = RestClient.builder(elasticNodes);
	        
	        
	     //       securityManager.configureSecurity(clientBuilder,null);
	        

	        RestHighLevelClient elasticClient = new RestHighLevelClient(clientBuilder);

	        return elasticClient;
	    }

	 
public Node[] getElastisearchHosts(List<String> hosts) throws MalformedURLException {
        
        Node[] elasticHosts = new Node[hosts.size()];
        int i = 0;
        
        for (String url : hosts) {
            
            URL elasticURL = new URL(url);
            
            elasticHosts[i] = new Node(
                    new HttpHost(elasticURL.getHost(), elasticURL.getPort(), elasticURL.getProtocol()));
            i++;
        }
        return elasticHosts;
    }

private List<Path>  collectPaths(String pathParentDir,String fileSuffix) {

	List<Path> pathsCollector=null;
	try {
		Path componentsDirectory = Paths.get(pathParentDir);

		pathsCollector = Files.walk(componentsDirectory,1,FileVisitOption.FOLLOW_LINKS)
				.filter(path->path.toAbsolutePath().toString().endsWith(fileSuffix))
				.collect(Collectors.toList());

		pathsCollector.remove(componentsDirectory);
		
	} catch (Exception e) {
		logger.error("Failed to collect paths",e);
	}
	
	
	return pathsCollector;
}



public void updateComponentTemplates() {
	List<Path> componentTemplatesPaths = collectPaths(componentTemplatesPath, ".json");
	if(componentTemplatesPaths==null) {
		logger.warn("No component templates available, skipping!");
		return;
	}
	UpdateComponentTemplateScript script = new UpdateComponentTemplateScript(elasticsearchUrl);
	for(Path aComponentPath:componentTemplatesPaths) {
		runApiCommandByScript(aComponentPath, script);
	}
}

public void updateIlms() {
	List<Path> ilmPoliciesPaths = collectPaths(ilmPoliciesPath, ".json");
	if(ilmPoliciesPaths==null) {
		logger.warn("No ilm policies available, skipping!");
		return;
	}
	UpdateIlmPolicyScript script = new UpdateIlmPolicyScript(elasticsearchUrl);

	for(Path ilmPath:ilmPoliciesPaths) {
		runApiCommandByScript(ilmPath, script);
	}
}

public void updateTemplates() {
	List<Path> templatesPaths = collectPaths(templatesPath, ".json");
	if(templatesPaths==null) {
		logger.warn("No  templates available, skipping!");
		return;
	}
	UpdateTemplateScript script = new UpdateTemplateScript(elasticsearchUrl);
	for(Path aTemplatePath:templatesPaths) {
		runApiCommandByScript(aTemplatePath, script);
	}
}

public void runApiCommandByScript(Path jsonFilePath,BashScript script) {

	String jsonFileName = jsonFilePath.getFileName().toString();

	File tempFile = new File("/tmp/"+jsonFileName);

	File bashScriptFile = null;

	try {

		InputStream jsonStream = new FileInputStream(new File(jsonFilePath.toAbsolutePath().toString()));

		copyInputStreamToFile(jsonStream, tempFile);	        

		String objectId = jsonFileName.replaceAll("\\.json", "");

		bashScriptFile = script.createTemporaryUpdateScript(objectId);

		ProcessBuilder builder = new ProcessBuilder();

		builder.command("bash", bashScriptFile.toString());

		builder.directory(new File("/tmp/"));

		Process process = builder.start();

		int exitCode = process.waitFor();

		if (exitCode != 0) {
			
			logger.warn("Failed to execute import saved object script");

		} else {
			logger.info(" {} imported ",jsonFileName);

		}



	} catch (Exception e) {

		logger.error("Failed to import  {}",jsonFileName,e);


	}finally {
		tempFile.delete();
		if(bashScriptFile!=null) {
			bashScriptFile.delete();
		}
	}
}

/**
 * Copy input stream to file.
 *
 * @param inputStream the input stream
 * @param file the file
 * @throws IOException Signals that an I/O exception has occurred.
 */
protected void copyInputStreamToFile(InputStream inputStream, File file)throws IOException {
	int DEFAULT_BUFFER_SIZE = 8192;
	// append = false
	try (FileOutputStream outputStream = new FileOutputStream(file, false)) {
		int read;
		byte[] bytes = new byte[DEFAULT_BUFFER_SIZE];
		while ((read = inputStream.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}
	}

}

}
