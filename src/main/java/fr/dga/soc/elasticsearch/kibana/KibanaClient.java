/**
 * 
 */
package fr.dga.soc.elasticsearch.kibana;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.RuntimeDelegate;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dga.soc.utils.ElasticsearchConfig;
import fr.dga.soc.utils.JsonUtils;
import fr.dga.soc.utils.SparkClientConfig;




// TODO: enrich progressively Javadoc
/**
 * The Class KibanaClient.
 *
 * @author pcaparroy
 */
public class KibanaClient implements Serializable{
	
	/** The client. */
	private transient Client client;
	
	/** The index patterns api. */
	private transient WebTarget indexPatternsApi;
	
	/** The spaces api. */
	private transient WebTarget spacesApi;
	
	/** The roles api. */
	private transient WebTarget rolesApi;
	
	/** The bulk saved ojects api. */
	private transient WebTarget bulkSavedOjectsApi;
	
	/** The find saved ojects api. */
	private transient WebTarget findSavedOjectsApi;
	
	/** The json mapper. */
	private transient ObjectMapper jsonMapper;
	
	/** The kibana URL. */
	public String kibanaURL;
	
	/** The kibana host. */
	public String kibanaHost;
	
	/** The kibana version. */
	public String kibanaVersion;
	
	/** The kibana user. */
	private String kibanaUser;
	
	/** The kibana password. */
	private String kibanaPassword;
	
	/** The authentication enabled. */
	private boolean authenticationEnabled;
	
	
	
	/** The logger. */
	Logger logger = LoggerFactory.getLogger(KibanaClient.class);
	
	/**
	 * Instantiates a new kibana client.
	 *
	 * @param config the config
	 * @param collector the collector
	 */
	public KibanaClient(ElasticsearchConfig config) {
		
		this(config.getKibanaUrl(),config.getKibanaVersion());
		
	}

	/**
	 * Instantiates a new kibana client.
	 */
	public KibanaClient() {
 
	}
	
	
	
	/**
	 * Instantiates a new kibana client.
	 *
	 * @param kibanaURL the kibana URL
	 * @param kibanaVersion the kibana version
	 * @param collector the collector
	 */
	public KibanaClient(String kibanaURL,String kibanaVersion) {
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
 
	    this.kibanaURL=kibanaURL;
		 this.kibanaVersion=kibanaVersion;
		 this.kibanaHost=kibanaURL;
		 
	}
	

	
	/**
	 * Gets the kibana URL.
	 *
	 * @return the kibanaURL
	 */
	public String getKibanaURL() {
		return kibanaURL;
	}

	/**
	 * Initialize.
	 *
	 * @throws Exception the exception
	 */
	public void initialize() throws Exception{
		if(kibanaURL!=null && client==null){
		    RuntimeDelegate.setInstance(new org.glassfish.jersey.internal.RuntimeDelegateImpl());
			 ClientBuilder builder = ClientBuilder.newBuilder().register(MultiPartFeature.class);
			 
			/* if(this.authenticationEnabled){
				 Path caCertificatePath = Paths.get(certificateAutorityPath);
					CertificateFactory factory =
					    CertificateFactory.getInstance("X.509");
					
					KeyStore trustStore = KeyStore.getInstance("pkcs12");
					trustStore.load(null, null);
					try (InputStream is = Files.newInputStream(caCertificatePath)) {
					    Iterator<? extends Certificate> trustedCertificates =
					    		factory.generateCertificates(is).iterator();
					    
					    while(trustedCertificates.hasNext()) {
					    	X509Certificate cert = (X509Certificate) trustedCertificates.next();
					    	trustStore.setCertificateEntry(cert.getSubjectDN().toString(), cert);
					    }
					}
					
					SSLContextBuilder sslContextBuilder = SSLContexts.custom()
					    .loadTrustMaterial(trustStore, null);
				
					final SSLContext sslContext = sslContextBuilder.build();
				 
					this.client = builder.sslContext(sslContext).hostnameVerifier(new HostnameVerifier() {
					
					@Override
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
				}).build();
			 }*/
			 
			 this.client =builder.build();
			 this.rolesApi =client.target(kibanaURL+KibanaApi.ROLES_PATH); 
			this.spacesApi =client.target(kibanaURL+KibanaApi.SPACES_PATH); 
			this.indexPatternsApi=client.target(kibanaURL+KibanaApi.INDEX_PATTERN_PATH);
			this.bulkSavedOjectsApi=client.target(kibanaURL+KibanaApi.BULK_SAVED_OBJECTS_PATH);
			this.findSavedOjectsApi=client.target(kibanaURL+KibanaApi.FIND_SAVED_OBJECTS_PATH);
			this.jsonMapper=new ObjectMapper();
		}
	}
	
	
	/**
	 * Gets the api.
	 *
	 * @param apiPath the api path
	 * @param spaceId the space id
	 * @return the api
	 */
	private WebTarget getApi(String apiPath,String[] spaceId){
        WebTarget api=null;
        if(spaceId==null || spaceId.length==0){
            api = client.target(kibanaURL+apiPath);
        }else{
            api = client.target(kibanaURL+"/s/"+spaceId[0]+apiPath);
        }
        return api;
    }
    
	/**
	 * Gets the import saved objects api.
	 *
	 * @param spaceId the space id
	 * @return the import saved objects api
	 */
	private WebTarget getImportSavedObjectsApi(String[] spaceId){
        WebTarget api=getApi(KibanaApi.SAVED_OBJECTS_import_PATH, spaceId);
        
        return api;
    }
	
	/**
	 * Gets the space api.
	 *
	 * @param spaceId the space id
	 * @return the space api
	 */
	private WebTarget getSpaceApi(String spaceId){
        WebTarget api=client.target(kibanaURL+KibanaApi.SPACES_PATH).path(spaceId);
        
        return api;
    }
	
	/**
	 * Gets the index pattern api.
	 *
	 * @param spaceId the space id
	 * @param patternId the pattern id
	 * @return the index pattern api
	 */
	private WebTarget getIndexPatternApi(String[] spaceId,String patternId){
		WebTarget ipApi=getApi(KibanaApi.INDEX_PATTERN_PATH, spaceId).path(patternId);
		
		return ipApi;
	}
	
	/**
	 * Gets the find saved objects api.
	 *
	 * @param spaceId the space id
	 * @return the find saved objects api
	 */
	private WebTarget getFindSavedObjectsApi(String[] spaceId){
        WebTarget fsoApi=getApi(KibanaApi.FIND_SAVED_OBJECTS_PATH, spaceId);
        
        return fsoApi;
    }
	
	/**
	 * Invoke.
	 *
	 * @param aTarget the a target
	 * @return the invocation. builder
	 */
	private Invocation.Builder invoke(WebTarget aTarget){
		
		
		Invocation.Builder builder =  aTarget.request(MediaType.APPLICATION_JSON)
				.header("Host", kibanaHost)
				.header("kbn-version",kibanaVersion)
				.header("Origin", kibanaURL)
				.header("Referer", kibanaURL+"/app/kibana")
				.header("kbn-xsrf", true);
		
		if(this.authenticationEnabled){
			
			String authToken = kibanaUser+":"+kibanaPassword;
			
			String base64AuthToken = "Basic "+Base64.getEncoder().encodeToString(authToken.getBytes());
			
			builder = builder.header("Authorization", base64AuthToken);
		
		}
		return builder;
	}
	

	/**
	 * Search index patterns.
	 *
	 * @param patternSeed the pattern seed
	 * @param spaceId the space id
	 * @return the list
	 */
	public List<String> searchIndexPatterns(String patternSeed,String... spaceId){
		
		List<String> patternsIds= new ArrayList<String>();
		
		try {
			
		    initialize();
			
			WebTarget myPatternTarget = getFindSavedObjectsApi(spaceId).queryParam("type", "index-pattern")
                    					.queryParam("search_fields", "title")
                    					.queryParam("search", patternSeed);
				
			
			Response response = (invoke(myPatternTarget)).get();
			
			logger.debug("response status {}",response.getStatus());
			
			if(response.getStatus()==200){
				
			    String responseStr = response.readEntity(String.class);
				
				JSONObject json = new JSONObject(responseStr);
				
				JSONArray savedObjects = json.optJSONArray("saved_objects");
				
				if(savedObjects!=null){
					
				    for (int i = 0; i < savedObjects.length(); i++) {
						
				        JSONObject indexPatternObject = savedObjects.getJSONObject(i);
						
						String patternId= indexPatternObject.optString("id");
						//if(patternSeed.contains(patternId)){
						patternsIds.add(patternId);
						
						logger.info("Pattern {} retrieved in kibana saved object with id {}",patternSeed,patternId);
						//}
					}
				}
					
				
			}else{
				logger.debug("Failed to retrieve Index Pattern {}: kibana response status is {}",patternSeed,response.getStatus());
			}
			//logger.debug(response.readEntity(String.class));
		} catch (Exception e) {

			logger.error("Failed to retrieve Index pattern {}",patternSeed,e);
		}
		
		return patternsIds;
	}
	
/**
 * Gets the space by id.
 *
 * @param patternId the pattern id
 * @return the space by id
 */
public boolean getSpaceById(String patternId){
		
		boolean patternFound=false;
		try {
			initialize();
			
			WebTarget myPatternTarget = spacesApi.path(patternId);
				
			//String queryParams = jsonMapper.writeValueAsString(new IndexPattern(pattern, pattern, timeFieldName));
			
			Response response = invoke(myPatternTarget).get();
			
			logger.debug("response status {}",response.getStatus());
			
			if(response.getStatus()==200){
				patternFound=true;
				logger.debug("Space {} retrieved",patternId);
			}else{
				logger.debug("Index Pattern {} does not exists",patternId);
				//logger.warn(response.readEntity(String.class));
			}
			
		} catch (Exception e) {

			logger.error("Failed to retrieve Kibana space {}",patternId,e);
		}
		
		return patternFound;
	}
	
	/**
	 * Gets the index pattern by id.
	 *
	 * @param patternId the pattern id
	 * @param spaceId the space id
	 * @return the index pattern by id
	 */
	public boolean getIndexPatternById(String patternId,String... spaceId){
		
		boolean patternFound=false;
		try {
			initialize();
			WebTarget myPatternTarget = getIndexPatternApi(spaceId,patternId);
				
			//String queryParams = jsonMapper.writeValueAsString(new IndexPattern(pattern, pattern, timeFieldName));
			
			Response response = invoke(myPatternTarget).get();
			
			//logger.debug("response status {}",response.getStatus());
			
			if(response.getStatus()==200){
				patternFound=true;
				logger.debug("Pattern {} retrieved",patternId);
			}else{
				logger.debug("Failed to retrieve Index Pattern {}: Response status: {}",patternId,response.getStatus());
			}
			logger.debug(response.readEntity(String.class));
		} catch (Exception e) {

			logger.error("Failed to create Index pattern {}",patternId,e);
		}
		
		return patternFound;
	}
	
	/**
	 * Reset index pattern.
	 *
	 * @param pattern the pattern
	 * @param timeFieldName the time field name
	 */
	public void resetIndexPattern(String pattern,String timeFieldName){
		clearIndexPatterns(pattern);
		createIndexPattern(pattern, timeFieldName,null);
	}
	
	
	
	/**
	 * Delete space.
	 *
	 * @param spaceId the space id
	 * @return true, if successful
	 */
	public boolean deleteSpace(String spaceId) {
	    boolean spaceDeleted=true;
        
        try {
            
            initialize();
            
            logger.info("deleting space {}",spaceId);
           
            WebTarget mySpaceTarget = getSpaceApi(spaceId);
            
            Response response = invoke(mySpaceTarget).delete();
           
            if(response.getStatus()==200){
            
                logger.info("Space {} deleted",spaceId);
            
            }else{
            
                logger.info("Space {} does not exists ",spaceId);
                logger.debug("response status: {} ",response.getStatus());

            }
            
        } catch (Exception e) {
            

            logger.error("Failed to delete Index patterns with id  {}",spaceId,e);
            spaceDeleted=false;
        }
        return spaceDeleted;
	}
	
	/**
	 * Delete index pattern by id.
	 *
	 * @param patternId the pattern id
	 * @param spaceId the space id
	 * @return true, if successful
	 */
	public boolean deleteIndexPatternById(String patternId,String... spaceId){
		boolean indexPatternDeleted=true;
		
		try {
			initialize();
			
			
				WebTarget myPatternTarget = getIndexPatternApi(spaceId, patternId);
				
				Response response = invoke(myPatternTarget).delete();
				if(response.getStatus()==200){
					logger.info("Pattern {} deleted",patternId);
				}else{
					logger.debug("Pattern {} does not exists response status: {} reason: {}",patternId,response.getStatus(),response.getStatusInfo().getReasonPhrase());
				}
				
			
			
		} catch (Exception e) {

			logger.error("Failed to delete Index patterns with id  {}",patternId,e);
			indexPatternDeleted=false;
		}
		return indexPatternDeleted;
	}
	
	/**
	 * Clear index patterns.
	 *
	 * @param patternSeed the pattern seed
	 * @param spaceId the space id
	 * @return true, if successful
	 */
	public boolean clearIndexPatterns(String patternSeed,String... spaceId){
		boolean indexPatternDeleted=true;
		
		try {
			initialize();
			
			List<String> patternsToDelete = searchIndexPatterns(patternSeed,spaceId);
			
			for(String patternId:patternsToDelete){
				WebTarget myPatternTarget = getIndexPatternApi(spaceId, patternId);
				
				Response response = invoke(myPatternTarget).delete();
				if(response.getStatus()==200){
					logger.info("Pattern {} deleted",patternId);
				}else{
					logger.warn("Failed to delete Index Pattern {} response status: {} reason: {}",patternId,response.getStatus(),response.getStatusInfo().getReasonPhrase());
				}
				
			}
			
		} catch (Exception e) {

			logger.error("Failed to delete Index patterns with seed  {}",patternSeed,e);
			indexPatternDeleted=false;
		}
		return indexPatternDeleted;
	}
	
	/**
	 * Creates the space if not exists.
	 *
	 * @param spaceId the space id
	 * @param name the name
	 * @return true, if successful
	 */
	public boolean createSpaceIfNotExists(String spaceId,String name){//, String desc,String initials,String image,String color,List<String> disabledFeatures){
		
		boolean patternCreated=false;
		
		try {
			
			initialize();
					
				WebTarget myPatternTarget = spacesApi;
				
				String[] df = null;
				
				/*if(disabledFeatures!=null){
					
					df=disabledFeatures.toArray(new String[disabledFeatures.size()]);
				
				}*/
				if(!getSpaceById(spaceId)) {
					KibanaSpace space = new KibanaSpace(spaceId,name);				
					
					String queryParams = jsonMapper.writeValueAsString(space);
					
					//createKibanaEntityByScript(createSpaceScript(queryParams));
					
					if(createKibanaEntity(myPatternTarget, queryParams)) {
					    patternCreated=true;
						logger.debug("Kibana space {} created",name);
					}else {
						logger.warn("Kibana space {} not created",name);
					}
				}else {
				    patternCreated=true;
					logger.debug("Kibana space {} allready exists",name);
				}
				
			
		} catch (Exception e) {
			logger.error("Failed to create Kibana space {}",spaceId,e);
			
		}
		
		return patternCreated;
	}	
	
	
	
	
	
	/**
	 * Generate ingestion report.
	 *
	 * @param executionDate the execution date
	 * @param fileDate the file date
	 * @param dataType the data type
	 * @param executionId the execution id
	 * @param cyberpediaAdminSpaceId the cyberpedia admin space id
	 * @return true, if successful
	 */
	public boolean generateIngestionReport(String executionDate,String fileDate,String dataType,String executionId,String cyberpediaAdminSpaceId) {
	    boolean imported=true;
        
	    String templateId = "cyberpedia-ingestion-report";
	    
        File savedObjectFile = new File("/tmp/"+templateId+".ndjson");
        try {
            
            initialize();
            
            InputStream savedObjectStream = this.getClass().getResourceAsStream("/saved-objects/cyberpedia-ingestion-report.ndjson");
            
            
            String text = JsonUtils.asJsonString(savedObjectStream);
            
            
            
            copyInputStreamToFile(new ByteArrayInputStream(text.getBytes()), savedObjectFile);
            
            
            
            File script = createImportSavedObjectScript(templateId, cyberpediaAdminSpaceId);
            
            
            ProcessBuilder builder = new ProcessBuilder();
            //builder.inheritIO();
            builder.command("bash", script.toString());

            builder.directory(new File("/tmp/"));
            Process process = builder.start();

            int exitCode = process.waitFor();

            if (exitCode != 0) {
                logger.warn("Failed to execute import saved object script");
                imported = false;
            } else {
                logger.info("saved object {} imported ",templateId);

            }
            
            
            
        } catch (Exception e) {
            imported=false;
            logger.error("Failed to import saved object {}",templateId,e);

            
        }finally {
           savedObjectFile.delete();
            
        }
        
        
        return imported;
	}
	
	
	
	/**
	 * Import saved object.
	 *
	 * @param objectId the object id
	 * @param spaceId the space id
	 * @return true, if successful
	 */
	public boolean importSavedObject(String objectfilePath,String objectFileName,String...spaceId) {

		boolean imported=true;	    
	   
	    File savedObjectFile = new File("/tmp/"+objectFileName);
	    try {
            
	        initialize();
	        
	        InputStream savedObjectStream = new FileInputStream(new File(objectfilePath));
	        
	        copyInputStreamToFile(savedObjectStream, savedObjectFile);	        
	        
	        File script = createImportSavedObjectScript(objectFileName, spaceId);
	        
	        ProcessBuilder builder = new ProcessBuilder();

            builder.command("bash", script.toString());

            builder.directory(new File("/tmp/"));

            Process process = builder.start();

            int exitCode = process.waitFor();

            if (exitCode != 0) {
                logger.warn("Failed to execute import saved object script");
                imported = false;
            } else {
                logger.info("saved object {} imported ",objectFileName);

            }
	        
            
	        
        } catch (Exception e) {
            imported=false;
            logger.error("Failed to import saved object {}",objectFileName,e);

            
        }finally {
           savedObjectFile.delete();
            
        }
	    
	    
	    return imported;
	}
	
	/**
	 * Creates the import saved object script.
	 *
	 * @param savedObjectId the saved object id
	 * @param spaceId the space id
	 * @return the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public File createImportSavedObjectScript(String savedObjectFileName,String...spaceId) throws IOException {
        File tempScript = File.createTempFile("script", null);

        Writer streamWriter = new OutputStreamWriter(new FileOutputStream(tempScript));
        PrintWriter printWriter = new PrintWriter(streamWriter);

        printWriter.println("#!/bin/bash");
       
        String apiURL = getImportSavedObjectsApi(spaceId).getUri().toURL().toString(); 

        printWriter.println("curl  -X POST "+apiURL+"?overwrite=true -H \"kbn-xsrf: true\" --form file=@"+savedObjectFileName);

       
        printWriter.close();

        return tempScript;
    }
	
	/**
	 * Copy input stream to file.
	 *
	 * @param inputStream the input stream
	 * @param file the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private  void copyInputStreamToFile(InputStream inputStream, File file)throws IOException {
	    int DEFAULT_BUFFER_SIZE = 8192;
        // append = false
        try (FileOutputStream outputStream = new FileOutputStream(file, false)) {
            int read;
            byte[] bytes = new byte[DEFAULT_BUFFER_SIZE];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        }

    }
	
	
	/**
	 * Creates the index pattern.
	 *
	 * @param patternId the pattern id
	 * @param timeFieldName the time field name
	 * @param patternAttributes the pattern attributes
	 * @param spaceId the space id
	 * @return true, if successful
	 */
	public boolean createIndexPattern(String patternId,String timeFieldName,Map<String,String> patternAttributes,String... spaceId){
		
	    boolean patternCreated=true;
		try {
			
			initialize();
					
			if(!getIndexPatternById(patternId, spaceId)) {
				
			    WebTarget myPatternTarget = getIndexPatternApi(spaceId,patternId);
				
					
				String queryParams = jsonMapper.writeValueAsString(new KibanaIndexPattern(patternId, patternId, timeFieldName,patternAttributes));
				
				boolean patterncreated = createKibanaEntity(myPatternTarget, queryParams);
				
				if(patternCreated) {
					
				    if(spaceId!=null && spaceId.length==1) {
					    
					    logger.debug("Kibana index pattern {} created in space {}",patternId,spaceId[0]);
					
					}else {
					
					    logger.debug("Kibana index pattern {} created",patternId);

					}
					
				}else {
				    
					logger.warn("Failed to create Kibana index pattern {} ",patternId);
				
				}
			
			}
		} catch (Exception e) {

            logger.error("Failed to create Index pattern {}",patternId,e);
			
		}
		
		return patternCreated;
	}
	
	
	
	/**
	 * Creates the kibana entity.
	 *
	 * @param myPatternTarget the my pattern target
	 * @param queryParams the query params
	 * @return true, if successful
	 */
	public boolean createKibanaEntity(WebTarget myPatternTarget,String queryParams){
		boolean entityCreated=false;
		try {
			
				
				Entity<String> jsonEntity = Entity.json(queryParams);
				
				Response response = invoke(myPatternTarget).post(jsonEntity);
				
				
				
				
				if(response.getStatus()==200){
					
				    logger.debug("Kibana entity created");
					entityCreated=true;
				}else{
					
				    String responseStr = response.readEntity(String.class);
					
				
				
					logger.warn("Failed to create Kibana entity kibana response code: {}",responseStr);
										
				}
			
			
		} catch (Exception e) {
		    

			logger.error("Failed to create Kibana entity {}",e);
			
		}
		
		return entityCreated;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		try {

			Path configPath = Paths.get("/etc/elastioc/config.yml");
			Constructor constructor = new Constructor(SparkClientConfig.class);
			Yaml yaml = new Yaml(constructor);
			SparkClientConfig config =  yaml.load(new FileInputStream(configPath.toFile()));
			
			KibanaClient kClient = new KibanaClient(config.getElasticsearch());			
			
			kClient.initialize();
			
			//kClient.importSavedObject("auditd.7.15.0.soc");
			
			//kClient.createSpace("administration", "administration");
			//kClient.clearIndexPatterns(null, new String[] {"spaceid"});
			//kClient.createIndexPattern("scan-space", "scan_time", null, "default");
			//kClient.getSpaceById("default");
            /*
             * //, "", "A", null,
             * null, null);
             * kClient.createIndexPattern("cyberpedia.ips.2019-08-01",IndexMappings.
             * AGGREGATION_TIMESTAMP,null);
             * kClient.deleteIndexPatternById("cyberpedia.ips.2019-08-01");
             * kClient.createIndexPattern("cyberpedia.scans.2019-08-02-v0",IndexMappings.
             * PROCESSING_TIME,null);
             * kClient.deleteIndexPatternById("cyberpedia.scans.2019-08-02-v0");
             * kClient.createIndexPattern("cyberpedia.scans.*", "scan_time", null);
             * kClient.deleteIndexPatternById("cyberpedia.scans.*");
             */
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		
	}

   

}
