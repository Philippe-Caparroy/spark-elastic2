/**
 * 
 */
package fr.dga.soc.elasticsearch.kibana;

import java.util.HashMap;
import java.util.Map;

// TODO: enrich progressively Javadoc
/**
 * The Class IndexPattern.
 *
 * @author pcaparroy
 */

public class IndexPattern {
	
	/** The attributes. */
	public Map<String,Object> attributes;
	//public boolean overwrite=false;
//	public String type="index-pattern";
	//public String id;
	/**
	 * Instantiates a new index pattern.
	 */
	public IndexPattern() {
		this.attributes = new HashMap<String, Object>();
		//this.overwrite=true;
	}

	/**
	 * Instantiates a new index pattern.
	 *
	 * @param title the title
	 * @param id the id
	 * @param timeFieldName the time field name
	 * @param additionalAttributes the additional attributes
	 */
	public IndexPattern(String title, String id, String timeFieldName,Map<String,String> additionalAttributes) {
		this();
		attributes.put("timeFieldName", timeFieldName);
		attributes.put("fields", "[]");
		attributes.put("title", title);
		if(additionalAttributes!=null){
			attributes.putAll(additionalAttributes);
		}
	}

	/**
	 * Gets the attributes.
	 *
	 * @return the attributes
	 */
	public Map<String, Object> getAttributes() {
		return attributes;
	}

	/**
	 * Sets the attributes.
	 *
	 * @param attributes the attributes to set
	 */
	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	/**
	 * @return the overwrite
	 */
	/*public boolean getOverwrite() {
		return overwrite;
	}

	*//**
	 * @param overwrite the overwrite to set
	 *//*
	public void setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
	}*/

	
	
	
	

}
