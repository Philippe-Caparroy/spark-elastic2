/**
 * 
 */
package fr.dga.soc.elasticsearch.kibana;

// TODO: enrich progressively Javadoc
/**
 * The Interface KibanaApi.
 *
 * @author pcaparroy
 */
public interface KibanaApi {
	
	/** The roles path. */
	public static String ROLES_PATH="/api/roles/role/";
	
	/** The spaces path. */
	public static String SPACES_PATH="/api/spaces/space";
	
	/** The index pattern path. */
	public static String INDEX_PATTERN_PATH="/api/saved_objects/index-pattern/";
	
	/** The SAVE D OBJECT S import PATH. */
	public static String SAVED_OBJECTS_import_PATH="/api/saved_objects/_import";
	
	/** The saved objects path. */
	public static String SAVED_OBJECTS_PATH="/api/saved_objects/";
	
	/** The bulk saved objects path. */
	public static String BULK_SAVED_OBJECTS_PATH="/api/saved_objects/_bulk_get";
	
	/** The find saved objects path. */
	public static String FIND_SAVED_OBJECTS_PATH="/api/saved_objects/_find";
}
