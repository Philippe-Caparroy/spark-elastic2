/**
 * 
 */
package fr.dga.soc.elasticsearch.scripts;

/**
 * @author pcaparroy
 *
 */
public class UpdateComponentTemplateScript extends BashScript {

	/**
	 * 
	 */
	public UpdateComponentTemplateScript(String elasticUrl) {

		super(elasticUrl);
		
	}

	@Override
	public String generateUpdateScriptCommand(String componentTemplateId) {
		 
		String apiURL = elasticUrl+"/_component_template/"+componentTemplateId; 

         return "curl  -X POST "+apiURL+" -H \"Content-Type: application/json\" -d @"+componentTemplateId+".json";

	}

}
