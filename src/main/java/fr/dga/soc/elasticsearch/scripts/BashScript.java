/**
 * 
 */
package fr.dga.soc.elasticsearch.scripts;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author pcaparroy
 *
 */
public abstract class BashScript {

	private Logger logger = LoggerFactory.getLogger(BashScript.class);
	
	protected String elasticUrl;
	/**
	 * 
	 */
	public BashScript(String elasticUrl) {
		this.elasticUrl = elasticUrl;
	}

	public File createTemporaryUpdateScript(String objectId) throws IOException {
        
		File tempScript = File.createTempFile("script", null);

        try {
        	
            Writer streamWriter = new OutputStreamWriter(new FileOutputStream(tempScript));
            PrintWriter printWriter = new PrintWriter(streamWriter);

            printWriter.println("#!/bin/bash");
           
            String updateCommand = generateUpdateScriptCommand(objectId); 
            printWriter.println(updateCommand);

           
            printWriter.close();
		} catch (Exception e) {
			logger.error("Failed to generate component template update script",e);
		}
        

        return tempScript;
    }
	
	
	public abstract String generateUpdateScriptCommand(String fileName);
}
