/**
 * 
 */
package fr.dga.soc.elasticsearch.scripts;

/**
 * @author pcaparroy
 *
 */
public class UpdateTemplateScript extends BashScript {

	/**
	 * 
	 */
	public UpdateTemplateScript(String elasticUrl) {

		super(elasticUrl);
		
	}

	@Override
	public String generateUpdateScriptCommand(String templateId) {
		 
		String apiURL = elasticUrl+"/_index_template/"+templateId; 

         return "curl  -X POST "+apiURL+" -H \"Content-Type: application/json\" -d @"+templateId+".json";

	}

}
