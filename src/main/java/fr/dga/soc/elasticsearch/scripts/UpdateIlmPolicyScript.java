/**
 * 
 */
package fr.dga.soc.elasticsearch.scripts;

/**
 * @author pcaparroy
 *
 */
public class UpdateIlmPolicyScript extends BashScript {

	/**
	 * 
	 */
	public UpdateIlmPolicyScript(String elasticUrl) {

		super(elasticUrl);
		
	}

	@Override
	public String generateUpdateScriptCommand(String policyId) {
		 
		String apiURL = elasticUrl+"/_ilm/policy/"+policyId; 

         return "curl  -X PUT "+apiURL+" -H \"Content-Type: application/json\" -d @"+policyId+".json";

	}

}
