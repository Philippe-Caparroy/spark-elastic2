/**
 * 
 */
package fr.dga.soc.feeds.misp;

import java.io.ByteArrayOutputStream;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import fr.dga.soc.spark.SparkClient;
import fr.dga.soc.utils.MispConfig;

/**
 * @author pcaparroy
 *
 */
public class MispCommandLineClient {

	private Logger logger = LoggerFactory.getLogger(MispCommandLineClient.class);
	private String mispSshUser;
	private String mispSshPassword;
	private String mispHost;
	private int mispSshPort;
	private String mispUserId;
	private String downloadPath;
	/**
	 * 
	 */
	public MispCommandLineClient() {
	}
	
	public MispCommandLineClient(MispConfig mispConfig) {
		this(mispConfig.getMispSShUser(),
				mispConfig.getMispSShPassword(),
				mispConfig.getUrl(),
				mispConfig.getMispSShPort(),
				mispConfig.getMispAdminUserId(),
				mispConfig.getDownloadPath());
	}

	public MispCommandLineClient(String mispSshUser,
							String mispSshPassword,
							String mispUrl,
							int mispSshPort,
							String mispUserId,
							String downloadPath) {
		super();
		this.mispUserId=mispUserId;
		this.mispSshUser = mispSshUser;
		this.mispSshPassword = mispSshPassword;
		try {
			URL url = new URL(mispUrl);
			this.mispHost = url.getHost();
		} catch (Exception e) {
			logger.error("Malformed misp URL",e);
		}
		
		this.mispSshPort = mispSshPort;
		this.downloadPath=downloadPath;
	}
	
	public void runSshCommand(String command,boolean background) {
		 Session session = null;
		    ChannelExec channel = null;
		    
		    try {

		        session = new JSch().getSession(mispSshUser, mispHost, mispSshPort);
		        session.setPassword(mispSshPassword);
		        session.setConfig("StrictHostKeyChecking", "no");
		        session.connect();
		        
		        channel = (ChannelExec) session.openChannel("exec");
		        if(background) {
		        	channel.setCommand(command);
		        }else {
		        	channel.setCommand(command+ " ; echo Command-has-finished-with-code-$?");
		        }
		        	
		        ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
		        channel.setOutputStream(responseStream);
		        channel.connect();
		        while (channel.isConnected()) {
		            Thread.sleep(5000);
		            String responseString = new String(responseStream.toByteArray());
		            if(responseString.startsWith("Command-has-finished-with-code")) {
		            	System.out.println(responseString);
		            	channel.disconnect();
		            	channel=null;
		            	break;
		            }else if(background) {
		            	break;
		            }
		        }
		        
		        
		    }catch(Exception e) {
		    	logger.error("Failed to execute ssh command {}",command,e);
		    } finally {
		        if (session != null) {
		            session.disconnect();
		        }
		        if (channel != null) {
		            channel.disconnect();
		        }
		    }
	}
	
	public void copyFeed(String feedId,String date) {
		String sourcePath ="/downloads/"+feedId+"/"+date+"/";
		String targetPath="/import/"+feedId+"/";
		logger.info("copying files from {} to {}",sourcePath,targetPath);
		
		runSshCommand("cp "+sourcePath+"*.json "+targetPath,false);
		
	}
	
	public void clearFeedData(String feedId) {

		String targetPath="/import/"+feedId+"/";
		logger.info("clearing files from {}",targetPath);
		
		runSshCommand("rm "+targetPath+"*.json ",false);
		
	}
	
	public void importFeed(String feedId) {
		logger.info("Importing feed {}  events",feedId);
		//runSshCommand("chown -R www-data:www-data /import/");
		
		//runSshCommand("chmod -R a+rw /import/");
		//logger.info("Misp user rights affected!");
		
		logger.info("starting import!")	;
		runSshCommand("/var/www/MISP/app/Console/cake Server fetchFeed "+mispUserId+" "+feedId+" &",true);

		logger.info("Done");
		
	}
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		MispConfig mispConfig = SparkClient.loadConfig().get().getMisp();
		
		MispCommandLineClient feedImporter = new MispCommandLineClient(mispConfig);
		
		
		
		feedImporter.importFeed("5");;
		
	}

}
