/**
 * 
 */
package fr.dga.soc.feeds.misp;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.dga.soc.utils.JsonUtils;

/**
 * @author pcaparroy
 *
 */
public class FeedAnalyzer {

	private static Logger logger = LoggerFactory.getLogger(FeedAnalyzer.class);

	private List<String> iocsTypes;

	private String[] types = { "url", "md5", "sha1", "sha256", "domain", "filename", "ip-dst", "ip-src",
			"ip-dst|port" };

	/**
	 * 
	 */
	public FeedAnalyzer() {
		iocsTypes = Arrays.asList(types);
	}

	public void analyzeFeed(String basePath) {

		try {
			
			List<String> jsonPaths = Files.walk(Paths.get(new URI("file://" + basePath)))
					.map(p -> p.toAbsolutePath().toString())
					.filter(p -> p.endsWith("json") && !p.endsWith("manifest.json")).collect(Collectors.toList());

			Set<String> matchedTypes = new HashSet<>();

			for (String filePath : jsonPaths) {

				Optional<JSONObject> optFeed = JsonUtils.loadJsonObject(filePath);

				if (optFeed.isPresent()) {

					JSONObject theDailyFeed = optFeed.get().getJSONObject("Event");

					String date = theDailyFeed.getString("date");

					JSONArray iocAttributes = null;

					if (theDailyFeed.has("Object")) {

						JSONArray theIocs = theDailyFeed.getJSONArray("Object");

						for (int i = 0; i < theIocs.length(); i++) {

							JSONObject anIoc = theIocs.getJSONObject(i);

							iocAttributes = anIoc.getJSONArray("Attribute");

						}

					} else if (theDailyFeed.has("Attribute")) {

						iocAttributes = theDailyFeed.getJSONArray("Attribute");

					}

					if (iocAttributes != null) {

						analyseAttributes(iocAttributes, matchedTypes);

						logger.info("Feed loaded");

					} else {

						logger.warn("No iocs in feed");

					}

				}
			}
			
			matchedTypes.stream().forEach(type -> logger.info(type));
			
		} catch (Exception e) {
			
			logger.error("Failed to process feed ", e);
		
		}

	}

	private void analyseAttributes(JSONArray attributes, Set<String> typesCollector) {
		for (int j = 0; j < attributes.length(); j++) {

			JSONObject anIocAttribute = attributes.getJSONObject(j);

			String type = anIocAttribute.getString("type");
			typesCollector.add(type);
			if (iocsTypes.contains(type)) {

				String uuid = anIocAttribute.getString("uuid");

				String value = anIocAttribute.getString("value");

			}

		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		FeedAnalyzer analyzer = new FeedAnalyzer();

		analyzer.analyzeFeed(args[0]);

	}

}
