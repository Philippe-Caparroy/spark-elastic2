/**
 * 
 */
package fr.dga.soc.feeds.misp;

/**
 * @author pcaparroy
 *
 */
public class AttributeSearch {

	private int page=0;
	
	private int limit=100;
	
	private String value;
	
	private String type;
	
	private String[] tags = {"DEPRECATED"};
	
	/**
	 * 
	 */
	public AttributeSearch() {
	
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}
	
	

}
