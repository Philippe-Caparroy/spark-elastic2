/**
 * 
 */
package fr.dga.soc.feeds.misp;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/**
 * @author pcaparroy
 *
 */
public class InsecureHostnameVerifier implements HostnameVerifier {

	/**
	 * 
	 */
	public InsecureHostnameVerifier() {

	}

	/* (non-Javadoc)
	 * @see javax.net.ssl.HostnameVerifier#verify(java.lang.String, javax.net.ssl.SSLSession)
	 */
	@Override
	public boolean verify(String arg0, SSLSession arg1) {
		
		return true;
	}

}
