/**
 * 
 */
package fr.dga.soc.feeds.misp;

/**
 * @author pcaparroy
 *
 */
public class Feed {

	
	public String name;
	public String provider="abuse.ch";
	public String input_source="local";
	public boolean enabled=true;
	public boolean publish=false;

	public String url="/import/";
	public String source_format="misp";
	public String distribution="0";
	
	/**
	 * 
	 */
	public Feed() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getInput_source() {
		return input_source;
	}

	public void setInput_source(String input_source) {
		this.input_source = input_source;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSource_format() {
		return source_format;
	}

	public void setSource_format(String source_format) {
		this.source_format = source_format;
	}

	public String getDistribution() {
		return distribution;
	}

	public void setDistribution(String distribution) {
		this.distribution = distribution;
	}

	public boolean isPublish() {
		return publish;
	}

	public void setPublish(boolean publish) {
		this.publish = publish;
	}
	
	

}
