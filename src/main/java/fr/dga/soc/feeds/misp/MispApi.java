/**
 * 
 */
package fr.dga.soc.feeds.misp;

/**
 * @author pcaparroy
 *
 */
public interface MispApi {

	/** The roles path. */
	public static String FEEDS_PATH="/feeds";
	
	public static String ADD_FEED_PATH="/feeds/add";
	
	public static String UPDATE_FEED_PATH="/feeds/edit";
	
	public static String ATTRIBUTES_SEARCH_PATH="/attributes/restSearch";


}
