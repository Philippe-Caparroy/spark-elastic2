/**
 * 
 */
package fr.dga.soc.feeds.misp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.RuntimeDelegate;
import javax.ws.rs.client.Invocation;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.dga.soc.spark.SparkClient;
import fr.dga.soc.utils.MispConfig;
import fr.dga.soc.utils.SparkClientConfig;


/**
 * @author pcaparroy
 *
 */
public class FeedsDownloader {

	//misp key:MsRefIJYqBjexN8b9j6dNXQWKDaYolCvlRDGEFP5

	private static Logger logger = LoggerFactory.getLogger(FeedsDownloader.class);

	protected HttpClient client;
	protected HttpGet getMethod;
	private String downloadPath;
    private String proxyUri;

	/**
	 * 
	 */
	public FeedsDownloader(MispConfig config,String proxyUri) throws Exception{
		this(config.getDownloadPath(),proxyUri);
	}


	public FeedsDownloader(String downloadPath,String proxyUri) throws Exception{
		HttpClientBuilder clientBuilder = HttpClientBuilder.create();
		if(proxyUri!=null) {
			URL url = new URL(proxyUri);
			
			clientBuilder.setProxy(new HttpHost(url.getHost(), url.getPort()));
		}
		client = clientBuilder.build();
		System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
		RuntimeDelegate.setInstance(new org.glassfish.jersey.internal.RuntimeDelegateImpl());
		this.downloadPath=downloadPath;
		this.proxyUri=proxyUri;
	}

	private WebTarget getTarget(String url) throws Exception{
		SSLContext sc = SSLContext.getInstance("TLSv1.2");// Java 8

		System.setProperty("https.protocols", "TLSv1.2");// Java 8
		if(proxyUri!=null) {
	        System.setProperty("https_proxy", proxyUri);

		}
		TrustManager[] trustAllCerts = { new InsecureTrustManager() };
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HostnameVerifier allHostsValid = new InsecureHostnameVerifier();

		ClientBuilder clientBuilder = ClientBuilder.newBuilder();
        if(proxyUri!=null) {
        	
        	ClientConfig config = new ClientConfig();
        	config.property(ClientProperties.PROXY_URI, proxyUri);  
        	config.connectorProvider( new ApacheConnectorProvider() );
        	clientBuilder = clientBuilder.withConfig(config);
        	
        }

        
        WebTarget urlTarget = clientBuilder.sslContext(sc).hostnameVerifier(allHostsValid).build().target(url);


		return urlTarget;
	}

	public String getDownloadedPath(String feed,String date) {
		return downloadPath+feed+"/"+date+"/";
	}
	
	
	public List<String> downloadFeedForDate(String feed,LocalDate date) {

		List<String> uuids = new ArrayList<>();
		try {
			File dateDownloadDir = new File(getDownloadedPath(feed, date.format(DateTimeFormatter.ISO_DATE)));

			if(!dateDownloadDir.exists()) {
				dateDownloadDir.mkdir();


				logger.info("downloading abuse.ch {} feed for {}",feed,date.format(DateTimeFormatter.ISO_DATE));

				String feedUrl = "https://"+feed+".abuse.ch/downloads/misp/manifest.json";


				WebTarget feedDownloadApi = getTarget(feedUrl);

				Response response = feedDownloadApi.request(MediaType.APPLICATION_JSON).get();


				logger.debug("response status {}", response.getStatus());


				if (response.getStatus() == 200) {
					String responseStr = response.readEntity(String.class) ;
					JSONObject manifest = new JSONObject(responseStr);
					Iterator<String> keysIterator = manifest.keys();
					while (keysIterator.hasNext()) {
						String fileUuid = (String) keysIterator.next();
						JSONObject iocs = (JSONObject)manifest.get(fileUuid);

						String iocDateStr = iocs.getString("date");
						//System.out.println(iocDateStr);
						if(iocDateStr.equals(date.format(DateTimeFormatter.ISO_DATE))) {
							logger.info("file ref {}  for date : {}",fileUuid,iocDateStr);

							uuids.add(fileUuid);

						}

					}

					downloadFile(feed,date.format(DateTimeFormatter.ISO_DATE), "manifest");
					for(String uuid:uuids) {
						downloadFile(feed,date.format(DateTimeFormatter.ISO_DATE),uuid);
					}

				}else {
					logger.warn("Problem with feed url for {} response status {}",feed,response.getStatus());
				}
				// TODO: handle exception
			}else {
				String downloadPath = getDownloadedPath(feed, date.format(DateTimeFormatter.ISO_DATE));

				uuids = Files.walk(Paths.get(downloadPath)).map(p->p.toAbsolutePath().toString()).filter(p->p.endsWith(".json") && !p.endsWith("manifest.json")).map(p->p.substring(p.lastIndexOf("/")+1)).map(p->p.substring(0,p.indexOf("."))).collect(Collectors.toList());
			}
		} catch (Exception e) {
			logger.error("Failed to download feed {}",feed,e);
		}

		return uuids;
	}

	public void downloadFile(String feed,String date,String uuid) {
		ReadableByteChannel readableByteChannel = null;



		String uuidUrl="https://"+feed+".abuse.ch/downloads/misp/"+uuid+".json";
		try {
			logger.info("downloading {}",uuidUrl);



			FileOutputStream fileOutputStream = new FileOutputStream(getDownloadedPath(feed, date)+uuid+".json", false);
			FileChannel outputChannel = (FileChannel)Channels.newChannel(fileOutputStream);
			getMethod = new HttpGet(uuidUrl);
			// getMethod.addHeader("Range", "bytes=" + startDownloadBytes + "-" + stopDownloadBytes);

			HttpResponse response = client.execute(getMethod);

			InputStream fileStream = response.getEntity().getContent();

			readableByteChannel = Channels.newChannel(fileStream);

			outputChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);

			logger.info("done!");
		} catch (Exception e) {
			logger.error("Failed to download uuid {}",uuid,e);
		} finally {
			if (readableByteChannel != null) {
				try {
					readableByteChannel.close();
				} catch (Exception e2) {
					logger.error("Failed to close readable file channel", e2);
				}
			}
			releaseConnection();

		}



	}



	public void releaseConnection() {
		if (getMethod != null) {
			getMethod.releaseConnection();
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Optional<SparkClientConfig> config = SparkClient.loadConfig();

		if (config.isPresent()) {
			try {
				FeedsDownloader downloader = new FeedsDownloader(config.get().getMisp(),config.get().getProxyUri());
				//	downloader.downlad("bazaar",LocalDate.parse("2022-02-03"));
				downloader.downloadFeedForDate("urlhaus",LocalDate.parse("2022-02-08"));
				//downloader.downlad("threatfox",LocalDate.parse("2022-02-03"));
			} catch (Exception e) {
				logger.error("",e);
			}
			
		}

	}

}
