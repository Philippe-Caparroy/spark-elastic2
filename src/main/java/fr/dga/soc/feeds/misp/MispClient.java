/**
 * 
 */
package fr.dga.soc.feeds.misp;

import java.io.File;
import java.io.FileReader;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.RuntimeDelegate;

import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.hash.Hashing;

import fr.dga.soc.elasticsearch.ElasticsearchManager;
import fr.dga.soc.spark.SparkClient;
import fr.dga.soc.utils.IocConfig;
import fr.dga.soc.utils.IocUtils;
import fr.dga.soc.utils.LogConfig;
import fr.dga.soc.utils.MaxmindGeolocProcessor;
import fr.dga.soc.utils.MispConfig;
import fr.dga.soc.utils.SparkClientConfig;

/**
 * @author pcaparroy
 *
 */
public class MispClient {

	static Logger logger = LoggerFactory.getLogger(MispClient.class);

	/** The client. */
	private transient Client client;

	private transient WebTarget feedsApi;

	private transient WebTarget addFeedApi;

	private transient WebTarget updateFeedApi;

	private transient WebTarget searchAttributesApi;

	/** The json mapper. */
	private transient ObjectMapper jsonMapper;

	/** The kibana URL. */
	public String mispURL;

	/** The kibana host. */
	public String authKey;
	/**
	 * 
	 */

	private MispCommandLineClient commandLineClient;

	

	public MispClient() {

	}

	public MispClient(MispConfig config) throws Exception{
		this(config.getUrl(), config.getAuthKey());
		this.commandLineClient = new MispCommandLineClient(config);
		
	}

	public MispClient(String mispURL, String authKey) {
		super();
		System.setProperty("sun.net.http.allowRestrictedHeaders", "true");

		this.mispURL = mispURL;
		this.authKey = authKey;

		try {
			initialize();

		} catch (Exception e) {
			logger.error("Failed to initialize Misp client", e);
		}
	}

	public void initialize() throws Exception {
		if (mispURL != null && client == null) {
			RuntimeDelegate.setInstance(new org.glassfish.jersey.internal.RuntimeDelegateImpl());

			SSLContext sc = SSLContext.getInstance("TLSv1.2");// Java 8

			System.setProperty("https.protocols", "TLSv1.2");// Java 8

			TrustManager[] trustAllCerts = { new InsecureTrustManager() };
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HostnameVerifier allHostsValid = new InsecureHostnameVerifier();

			ClientBuilder builder = ClientBuilder.newBuilder().sslContext(sc).hostnameVerifier(allHostsValid)
					.register(MultiPartFeature.class);

			this.client = builder.build();
			this.feedsApi = client.target(mispURL + MispApi.FEEDS_PATH);
			this.addFeedApi = client.target(mispURL + MispApi.ADD_FEED_PATH);
			this.searchAttributesApi = client.target(mispURL + MispApi.ATTRIBUTES_SEARCH_PATH);

			this.jsonMapper = new ObjectMapper();
		}
	}

	private Invocation.Builder invoke(WebTarget aTarget) {

		Invocation.Builder builder = aTarget.request(MediaType.APPLICATION_JSON).header("Authorization", authKey);

		return builder;
	}
	
	

	
	public Optional<List<String>> listDeprecatedAttributes(String attributeType){
		List<String> deprecatedAttributes = null;
		
		try {
			AttributeSearch aSearch = new AttributeSearch();
			aSearch.setType(attributeType);
			aSearch.setTags(new String[] {"DEPRECATED"});
			String queryParams = jsonMapper.writeValueAsString(aSearch);

			String response = postMispEntity(searchAttributesApi, queryParams);
			
			JSONObject resp = new JSONObject(response);
			
			JSONArray attributes = resp.getJSONObject("response").getJSONArray("Attribute");
			if(attributes.length()>0) {
				deprecatedAttributes=new ArrayList<>();
			}
			for (int i = 0; i < attributes.length(); i++) {

				JSONObject anAttr = attributes.getJSONObject(i);
				
				String value = anAttr.getString("value");
				deprecatedAttributes.add(value);
				
			}
			
			//logger.info(response);void
		} catch (Exception e) {
			logger.error("Failed to list deprecated attributes with type : {}",attributeType,e);
		}
		
		
		return Optional.ofNullable(deprecatedAttributes);
	}

	public Map<String,String> listFeeds() {

		 Map<String, String> feedsIdsByName = new HashMap<>();

		Response response = (invoke(feedsApi)).get();

		logger.info("response status {}", response.getStatus());

		if (response.getStatus() == 200) {

			String responseStr = response.readEntity(String.class);

			JSONArray jsonFeeds = new JSONArray(responseStr);

			for (int i = 0; i < jsonFeeds.length(); i++) {

				JSONObject feed = jsonFeeds.getJSONObject(i).getJSONObject("Feed");

				String name = feed.optString("name");

				if (name.length() > 0) {

					String id = feed.getString("id");

					logger.info("feed {} :  {}", id, name);

					feedsIdsByName.put(name, id);

				}

			}

		}
		
		return feedsIdsByName;
	}

	public String createFeed(String feedId) {

		logger.info("Creating misp feed {}", feedId);
		String mispId = null;

		try {

			Feed aFeed = new Feed();

			aFeed.setName(feedId + " local");

			aFeed.setUrl(aFeed.getUrl() + feedId + "/");

			String queryParams = jsonMapper.writeValueAsString(aFeed);

			String response = postMispEntity(addFeedApi, queryParams);

			JSONObject jsonResp = new JSONObject(response);

			mispId = jsonResp.getJSONObject("Feed").getString("id");

			updateFeedApi = client.target(mispURL + MispApi.UPDATE_FEED_PATH + "/" + mispId);

			response = postMispEntity(updateFeedApi, queryParams);

			logger.info("done!");
		} catch (Exception e) {

			logger.error("Failed to create feed {}", feedId, e);

		}
		return mispId;

	}

	public String postMispEntity(WebTarget myPatternTarget, String queryParams) {
		String responseStr = null;
		try {

			Entity<String> jsonEntity = Entity.json(queryParams);

			Response response = invoke(myPatternTarget).post(jsonEntity);

			responseStr = response.readEntity(String.class);

			if (response.getStatus() == 200) {

				logger.debug("Misp entity created");

			} else {

				logger.warn("Failed to create Misp entity, response code: {}", responseStr);

			}

		} catch (Exception e) {

			logger.error("Failed to create Misp entity {}", e);

		}

		return responseStr;
	}

	


	public MispCommandLineClient getCommandLineClient() {
		return commandLineClient;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Optional<SparkClientConfig> config = SparkClient.loadConfig();

		if (config.isPresent()) {
			try {
				MispConfig misp = config.get().getMisp();

				MispClient mispCli = new MispClient(misp);

				ElasticsearchManager elasticsearch = new ElasticsearchManager(config.get().getElasticsearch());

				
				// elasticsearch.updateComponentTemplates();
				 elasticsearch.updateTemplates();
				
				//mispCli.updateAllFeeds(false);
				
				//mispCli.generateIocTests(config.get().getIocs());
				
				//mispCli.updateFeeds("2021-03-13",false);
				// mispCli.createFakeLogIndex("urlhaus", "2022-02-03");
				//mispCli.listDeprecatedAttributes("md5");
			} catch (Exception e) {
				
				logger.error("Failed to instantiate Misp client", e);
				
			}
		}
	}

}
