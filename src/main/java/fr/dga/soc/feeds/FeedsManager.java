/**
 * 
 */
package fr.dga.soc.feeds;

import java.io.File;
import java.io.FileReader;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.elasticsearch.action.DocWriteRequest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.hash.Hashing;

import fr.dga.soc.elasticsearch.ElasticsearchManager;
import fr.dga.soc.feeds.misp.FeedsDownloader;
import fr.dga.soc.feeds.misp.MispClient;
import fr.dga.soc.utils.IocConfig;
import fr.dga.soc.utils.IocUtils;
import fr.dga.soc.utils.LogConfig;
import fr.dga.soc.utils.MaxmindGeolocProcessor;
import fr.dga.soc.utils.SparkClientConfig;

/**
 * @author pca
 *
 */
public class FeedsManager {

	static Logger logger = LoggerFactory.getLogger(FeedsManager.class);

	private FeedsDownloader feedsDownloader;
	private ElasticsearchManager elasticsearchManager;
	private MispClient mispClient;
	private MaxmindGeolocProcessor geolocProcessor;
	private boolean withMispImport;
	private boolean withFeedIndexing;
	final public static String DOWNLOAD_ACTION="download";
	final public static String UPDATE_MISP_ACTION="updateMisp";
	final public static String INDEX_IOC_ACTION="indexIoc";

	public FeedsManager(SparkClientConfig config) throws Exception{
		
		this.feedsDownloader = new FeedsDownloader(config.getMisp(),config.getProxyUri());
		this.withFeedIndexing=config.getElasticsearch().isFeedsIndexing();
		this.withMispImport=config.getMisp().isFeedsImport();
		this.elasticsearchManager = new ElasticsearchManager(config.getElasticsearch());
		this.mispClient = new MispClient(config.getMisp());
		if(withFeedIndexing && config.getMaxmind().getDatabasePath()!=null) {
			try {
				this.geolocProcessor = new MaxmindGeolocProcessor(config.getMaxmind().getDatabasePath());
			} catch (Exception e) {
				logger.error("Failed to instantiate maxmind geoloc processor",e);
			}
			
		}
	}

	

	public Set<String> updateAllFeeds(LocalDate startDate,String updateAction) {

		Set<String> iocTypesCollector = new HashSet<>();
		
		LocalDate stopDate= LocalDate.now();

		
		while (startDate.isBefore(stopDate) || startDate.isEqual(stopDate)) {

			updateFeeds(startDate.format(DateTimeFormatter.ISO_DATE), updateAction,iocTypesCollector);

			startDate = startDate.plus(1, ChronoUnit.DAYS);
		}
		
		return iocTypesCollector;

	}

	public void updateFeeds(String date, String updateAction ,Set<String> iocTypesCollector) {

		updateFeed("urlhaus", date, updateAction,iocTypesCollector);
		updateFeed("bazaar", date, updateAction,iocTypesCollector);
		updateFeed("threatfox", date, updateAction,iocTypesCollector);
		
	}

	public void updateFeed(String feed, String date, String updateAction,Set<String> iocTypesCollector) {

		switch (updateAction) {
		case DOWNLOAD_ACTION:
			List<String> fileIds = feedsDownloader.downloadFeedForDate(feed, LocalDate.parse(date));
			break;
		case UPDATE_MISP_ACTION:
			if(withMispImport) {
				logger.info("importing feed {} - {} into misp sharing platform", feed, date);

				Map<String,String> feedsIdsByName = mispClient.listFeeds();

				String mispFeedId = feedsIdsByName.get(feed + " local");

				if (mispFeedId == null) {

					mispFeedId = mispClient.createFeed(feed);

				} else {

					logger.info("Misp feed {} allready exists: skipping creation!",mispFeedId);

				}

				mispClient.getCommandLineClient().copyFeed(feed, date);

				mispClient.getCommandLineClient().importFeed(mispFeedId);
			}
			break;
		case INDEX_IOC_ACTION:
			indexFeed(feed, date,iocTypesCollector);
			break;

		default:
			break;
		}
		
		

	}
	
	
	
	
	
	public void indexFeed(String feed,String date,Set<String> iocTypesCollector){
		if(withFeedIndexing) {
			indexFeed(feed, date, "iocs-abuse.ch",iocTypesCollector);
		}
	}

	public void indexFeed(String feed,String date, String indexName,Set<String> iocTypesCollector) {
		try {
			
			String downloadedPath  =feedsDownloader.getDownloadedPath(feed, date);
			
			
			List<String> iocsPaths = Files.walk(Paths.get(downloadedPath)).map(p -> p.toAbsolutePath().toString())
					.filter(p -> (p.endsWith(".json") && !p.endsWith("manifest.json"))).collect(Collectors.toList());

			List<DocWriteRequest<?>> requests = new ArrayList<>();

			for (String iocPath : iocsPaths) {

				JSONObject aFeed = new JSONObject(new JSONTokener(new FileReader(new File(iocPath))));

				if(aFeed.getJSONObject("Event").has("Object")) {
					JSONArray iocs = aFeed.getJSONObject("Event").getJSONArray("Object");
	
					for (int i = 0; i < iocs.length(); i++) {
						
						JSONObject anIoc = iocs.getJSONObject(i);
	
						JSONArray attributes = anIoc.getJSONArray("Attribute");
	
						parseAttributes(feed,indexName, attributes, requests,iocTypesCollector);
	
					}
				}

				if(aFeed.getJSONObject("Event").has("Attribute")) {
					JSONArray iocs = aFeed.getJSONObject("Event").getJSONArray("Attribute");

					parseAttributes(feed,indexName, iocs, requests,iocTypesCollector);

				}
				elasticsearchManager.bulkIndex(requests);
				requests.clear();

			}

		}catch(

	Exception e)
	{
		logger.error("Failed to index feed {}", e);
	}

}
	
	private void parseAttributes(String feed,String indexName, JSONArray attributes, List<DocWriteRequest<?>> requests,Set<String> iocsTypesCollector) {

		for (int j = 0; j < attributes.length(); j++) {

			JSONObject anAttribute = attributes.getJSONObject(j);

			String type = anAttribute.getString("type");

			if (isIndexable(type)) {
				if(iocsTypesCollector!=null) {
					iocsTypesCollector.add(type);
				}
				String value = anAttribute.getString("value");
				String id = anAttribute.getString("uuid");
				
				Map<String, Object> ioc = new HashMap<>();
				ioc.put("uuid", id);
				ioc.put(type, value);
				ioc.put("origin", feed);
				ioc.put("@timestamp", anAttribute.getLong("timestamp")*1000L);
				
				if (IocUtils.needsSha256Conversion(type)) {
					String hashValue = Hashing.sha256().hashString((String) value, StandardCharsets.UTF_8).toString();
					ioc.put("ioc_sha256", hashValue);
				}
				
				if(type.equals("ip-dst|port")) {
					String[] split=value.split("\\|");
					ioc.put("ip-dst", split[0]);
					//ioc.put("port", split[1]);
				}
				
				Optional<InetAddress> address = getInetAddress(ioc, type);
				if(address.isPresent() && geolocProcessor!=null) {
					try {
					geolocProcessor.generateCityEnrichment(address.get(), ioc);
					}catch(Exception e) {
						logger.error("failed to process geoloc enrichment",e);
					}
				}
				
				if(type.equals("ip-dst|port")) {
					
					ioc.remove("ip-dst");
					
				}

				requests.add(elasticsearchManager.asUpdateRequest(indexName, ioc, id));
			}
		}
	}
	
	private boolean isIndexable(String type) {
		boolean isIndexable = false;

		switch (type) {
		case "md5":
			isIndexable = true;
			break;
		case "sha1":
			isIndexable = true;
			break;
		case "sha256":
			isIndexable = true;
			break;
		case "domain":
			isIndexable = true;
			break;
		case "url":
			isIndexable = true;
			break;
		case "ip-dst|port":
			isIndexable = true;
			break;
		case "ip-dst":
			isIndexable = true;
			break;
		case "ip-src":
			isIndexable = true;
			break;

		case "filename":
			isIndexable = true;
			break;
		default:
			isIndexable = false;
			break;
		}

		return isIndexable;
	}

	
	private Optional<InetAddress> getInetAddress(Map<String,Object> ioc,String type) {
		Optional<InetAddress> address=Optional.empty();
		try {
			switch (type) {
			case "ip-src":
				address = Optional.of(InetAddress.getByName((String)ioc.get(type)));
				break;
			case "ip-dst":
				address = Optional.of(InetAddress.getByName((String)ioc.get(type)));
				break;
			case "ip-dst|port":
				address = Optional.of(InetAddress.getByName((String)ioc.get("ip-dst")));
				break;
			default:
				break;
			}
		} catch (Exception e) {
			logger.error("Failed to obtain inetaddress for geoloc",e);
		}
		
		
		
		return address;
	}
	
public void generateIocTests(SparkClientConfig config,String iocType) {
		
	Map<String,IocConfig> iocConfigs = config.getIocs();
		
		for(Entry<String, IocConfig> entry:iocConfigs.entrySet()) {
			
			String iocName = entry.getKey();
			if(!iocName.startsWith("virus")) {
				
				IocConfig iocConfig = entry.getValue(); 
				
				String currentType = iocName.substring(iocName.indexOf("-")+1);
				
				Map<String,LogConfig> targetsConfigs = iocConfig.getTargets();
				
				LogConfig firstConfig = targetsConfigs.values().iterator().next();
				
				String indexPattern = firstConfig.getIndexPattern();
				
				String fieldName = firstConfig.getSha256FieldAlias();
				
				if(iocType!=null) {
					if(currentType.equals(iocType)) {
						long iocsNumber = elasticsearchManager.countIocs(currentType);
						
						elasticsearchManager.generateIocTests(currentType, iocsNumber,indexPattern.replaceAll("\\*", currentType+"-test"),fieldName);

					}
				}else {
					long iocsNumber = elasticsearchManager.countIocs(currentType);
					
					elasticsearchManager.generateIocTests(currentType, iocsNumber,indexPattern.replaceAll("\\*", currentType+"-test"),fieldName);

				}
				
				
				
			}
			
		}
		
	}

}
