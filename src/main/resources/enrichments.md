# Enrichments

<p>Raw scans ingested by Cyberpedia needs further processing to reveal all characteristics associated with an ip address (e.g products, vulnerabilities, etc...). This is the process of data enrichment.<br></br></p>   
    

## Overview

Enrichment of raw scan data is performed with the help of enrichment processors.
A processor can be defined with a json file containing it's description.
To generate an active pipeline of enrichments, enrichment processors files must be ingested by cyberpedia as raw data files.

## Structure of an enrichment processor

Enrichment of raw scan data relies on three to four steps:

* identifying scans that contains a particular information
	 
* eventually (optional) aggregating these scans with the help of an aggregation key: a common properties (e.g.	ip.address) 
	 
* extracting the information from the scans
	 
* generating a new/derived information:  the enrichment
	 
These steps are performed with the help of the two main components of the enrichment processor:

* The Indicator
	 
* The enrichment generator
	 
### Indicator

It indicates that the scan contains some information in one of it's field, that might lead to the production of an enrichment.

We chose to use the formalism of [stix-v2.1](./stix-v2.1-cs02.pdf#%5B%7B%22num%22%3A216%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C69%2C410%2C0%5D), because:
   
   * It's formalism is perfectly adapted to our needs
   
   * It allows to maintain a link with the Cyber ontology
   
The indicator contains a pattern, that evaluates to true/false and allow to select the scan for further processing.

The pattern is expressed in the [stix patterning language](./stix-v2.1-cs02.pdf#%5B%7B%22num%22%3A524%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C69%2C529%2C0%5D).
The reader can refer to the above link to a precise description of the concepts, and in the following we will illustrate it's application to some example of scan enrichments.

An indicator has a datasource, which in our case indicates the scan elasticsearch index.

### Enrichment generator	

The enrichment generator describes how the indications provided by the matched scan, are used to produce an enrichment. 
It must specify a datasink: in our case at the moment, all enrichments are stored in a daily enrichments index.

The enrichment generator can be made up of one ore more enrichment models.
Each model states how the indications found in the observered data are used to generate an enrichment, stored in a target field.

### A simple enrichment processor matching a favicon hash

This example illustrates how to model the simplest form of an enrichment processor.
Once ingested by cyberpedia, this processor will be **stored in the cyberpedia.processors.models index**, and can be visualized in Kibana.

```
{
	"name": "faviconHashes",
    "version": "0.1.2",
    "categories": ["tags"],
     
    "indicator": {
    	"pattern": {
    				"observation":{
							          "comparison": 
							               {
							                    "sourceField": "http.favicon.hash",
							                    "value": "-981606721",
							                    "operator": "=",
						                    		"indicates": ["hosting panels"]
							               },
		          						"dataSource": "scans"
		          						}
     				},
     "enrichmentGenerator":{
     				     		"dataSink":"enrichments",
					           "enrichmentModels":[{"targetField": "tags","addIndicator": true}],
					           "propagatedFields": ["ip.address"]
     							}
 }
```

* The name attribute is mandatory, it represents the natural identifier of the enrichment processor.  
*  A three part version number is mandatory. Updates of an enrichment processor requires version change. 
* An array of categories is optional, but allows to filter the enrichment processors, while examining them with Kibana.
  
  The indicator object must contains a pattern child object.
  According to the stix specification, the simplest form of pattern is one that targets a single field of a single observation (scan).
  The pattern is made up of a single Observation expression that indicates it's datasource (here the scans), and an array of  clauses, one per field condition.
  The operator attribute specify how the boolean clauses are combined (if not precised it is by default set to OR).

Here the array of clauses contains objects that  describe Stix Comparison expressions.

1. The source field must respect the syntax of the elasticsearch index schema.
2. The comparison expression will match all scans for which the source field value, compared using the provided operator to the reference value given matches.
3. the **indicates** field provides the meaning of such a successful comparison.

So here:  **when http.favicon.hash=-981606721 it indicates that the scan is related with the tag "hosting panels".**

In the current version of the enrichment processor implemented comparison operators are:

* =
* <
* <=
* >
* >=

At the moment, only integer values (e.g port) can be compared.

* MATCHES_ALL (existence)
* REGEXP (regular expression respecting perl syntax) on textual fields.


Each times a scan matches, an enrichment will be generated.

The enrichment generator must provide:

* A dataSink: here the enrichments index name.
* at least one enrichment model specifying
     * the target field name for the enrichment (the type is keyword by default in the current version).
     * the flag "addIndicator"=true if the matched indicator clause contains the enrichment value (Here it's the case).
     * alternatively, a list of one or more values (see later example)
     * Optionally, a prefix and/or a suffix that might be appended to the enrichment value.
* an array of propagated values that contains other source scan values that we want to associate with the scan (here the ip.address).

### Using regular expression

The following example illustrates how to use regular expressions, and multiple enrichment models inside a single enrichment processor.

```
{
  "name": "wordpress",
  "version": "0.1.0",
  "categories": ["tags","cpes"],
  "indicator": 
    {
    "pattern": {
    				"observation":{
					    				"dataSource": "scans",
					    				"comparisons":[
					    							{"sourceField": "http.bodyHtml",
					      							"operator": "MATCHES_REGEXP",
					      							"value": "<meta\\sname=\"generator\"\\scontent=\"WordPress\\s(\\d+\\.\\d+\\.?\\d{0,2})",
					      							"captureGroupIndex": "1"},
												    {
												      "sourceField": "http.bodyHtml",
												      "operator": "MATCHES_REGEXP",
												      "value": "\/wp-includes\/css\/dist\/block-library\/style\\.min\\.css?ver=(\\d+\\.\\d+\\.?\\d{0,2})",
												      "captureGroupIndex": "1"
												    }
					      							
					      							],
						               "filter":  {
										    "query": {
										      "bool": {
										        "should": [
										          {
										            "match_phrase": {
										              "http.bodyHtml": "<meta name=\"generator\" content=\"WordPress"
										            }
										          }]
										          }
										         }
										        }
										    }
      			}
   },  
  "enrichmentGenerator":{"dataSink": "enrichments",
					"propagatedFields": ["ip.address"],
					"enrichmentModels": [
					  				  {
									  	"targetField": "tags",
									  	"values": ["wordpress","cms"]
									  },
									  {"targetField": "cpes",
									   "prefix":"a:wordpress:wordpress:",
									    "addIndicator":true},
									    
									  {"targetField": "cpeUri",
									   "prefix":"a:wordpress:wordpress:",
									    "addIndicator":true,
									    "suffix": ":*:*:*:*:*:*:*"},
									    
									  {"targetField": "cpeUri",
									   "prefix":"a:wordpress:wordpress:",
									    "values": ["*:*:*:*:*:*:*:*"]}
					   ]
   }
}
```

Here the two comparison expressions used on the field "http.bodyHtml" uses the operator **MATCHES_REGEXP** and incorporate regular expressions in the **value** field.

**Regular expression must be json string escaped**

Because no operator is specified in the root pattern, the default disjunction operator (OR) is used.

A very important part of the indicator appears in this example, **the filter**.

This is a pre processing filter that will be used to select candidate scans, and avoid to run regular expressions on billion scans.
This filter must be expressed in form of an elasticsearch query, and should be prepared in kibana's dev console.

When simple comparison expressions are used (operators: =,<,>,etc...) the filter query is automatically generated behind the scene, except if a filter was specified in the root of the pattern.
But when regular expressions are used, it is not possible (at least simple) to convert the regexp into elasticsearch queries, due to differences between lunece regular expressions implementation and the full perl syntax.

* similarly to the previous **indicates* flag, if a capture group is used to capture information of the enrichment (e.g. version number), it(s index (>=1) must be provided.

**Multiple enrichment models**

Here multiple enrichment models are used in the specification of the enrichment generator.
They are self explanatory, and allow to :
* generate a tag field
* generate a cpe field by appending a prefix to the captured version number.
* generate two forms of cpeUri necessary for later cve matching by appending prefix and suffix.

### More complex examples

So far only one scan was involved in the indicator construction.
If multiple scans (observations) of a single ip.address must be considered to match an indication, we simply need to use distinct **Observation expressions** in the stix pattern.

#### Matching distinct fields on the same scan

To do so, only two comparison expressions must be associated  in the clauses array of the pattern.

```
{
"name": "test-single-scan",
"categories": ["multifields","test"],
"version": "1.0.0",
"indicator": 
    {
    "pattern": {
    	"observation":{
    					"dataSource": "scans",
    					"operator" :"AND",
			    		"comparisons":[{ 
						      				"sourceField": "http.favicon.hash",
						      				"operator": ">=",
						      				"value": "675000"
						    				},
						    				{
						    				"sourceField": "data",
						    				"operator": "MATCHES_REGEXP",
						    				"value": "edge.*"
						   					 }]
    				}
    	}
	},
	"enrichmentGenerator":{"dataSink": "enrichments",
					"propagatedFields": ["ip.address"],
					"enrichmentModels": [
					  				  {
									  	"targetField": "tags",
									  	"values": ["multifield-test"]
									  }]
   }
}


```
#### Multiples scans with common ip address

*matching a canon printer on an ip with port 1900 opened (distinct scans)*

Here we would like to find ip.address of a connected canon-printer although exposing an opened 1900 port.

Because scans are distinct observations, in time and also in termes of protocol and port, of ip.addresses, we need multiple scans (observations) to identify such an ip.

```
{
"name": "canon-printer",
"version": "1.0.0",
"categories": ["multifields","printers"],
"indicator": 
    {
    "pattern": {
		    	"operator" :"AND",
		    	"observations":
			    				[
			    				{"dataSource": "scans",
			    				"comparison":
					    				{ 
					      				"sourceField": "port",
					      				"operator": "=",
					      				"value": "1900"
					    				}
			   						
			    				},
			    				{"dataSource": "scans",
			    					"comparison": 
							    				{
							    				"sourceField": "data",
							    				"operator": "MATCHES_REGEXP",
							    				"value": "mf[0-9]{3,4}([a-z])?.*"
							   					 }
			    				}
		    				],
		    		"qualifier": {
		    					"aggs": {
									    "groupByIp": {
									      "terms": {
									          "field": "ip.address" 
									        }
									      }
									    }
								  }
							
	    			}
    	},

"enrichmentGenerator":{"dataSink": "enrichments",
							"propagatedFields": ["ip.address"],
							"enrichmentModels": [
					  				  {
									  	"targetField": "tags",
									  	"values": ["canon-printer"]
									  }]
   						}
}

```
Here the root pattern combines two **Observation expressions** that **must** be met (operator = AND) with the help of two clauses.
These first level clauses each specify their dataSource, and a nested clause which is a comparison expression.
One indicates the 1900 opened port, and the other indicates the presence of a canon printer  due to some text present in the data field.

The root pattern, further uses the qualifier field to specify that observations should be grouped by ip.address.

**NB: when multiple observation expressions are used in the pattern clause, it necessarily states that each pattern will match a distinct scan**.



### Python scripts

```
{
  "name": "language-detection",
  "enrichmentGenerator":{
					"enrichmentModels": [{"dataSink": "enrichments",
										"propagatedFields": ["ip.address"],
									  	"targetField": "language",
									  	"propagatedFields": ["language"]
									  }]
   },  
  "version": "0.1.0",
  "categories": ["language"],
  "filter":  {
   "query": {
    "exists": {
      "field": "http.bodyHtml"
    }
  }
  },
  "indicator": {
    "pattern": {
    	"observations":[
							{   "dataSource": "scans",
								"preprocessors": [{ "preprocessorId": "languagedetection.py","sourceField": "http.bodyHtml"}],
		    					"comparisons":[{"operator": "EXISTS", "sourceField": "http.bodyHtml"}]
	    					}	
    					]
   	}
   }
}


```
### Aggregations

*Hosting-provider*

```
{
"name": "hosting-provider",
"categories": ["multifields","aggregation","provider"],
"version": "1.0.0",
	
"enrichmentGenerator":{
					"enrichmentModels": [
					  				  {"dataSink": "enrichments",
										"propagatedFields": ["ip.address"],
									  	"targetField": "tags",
									  	"values": ["hosting-provider"]
									  }]
									  },
"indicator": 
    {
    "pattern": {
		    	"observations": [
			    				{"dataSource": "scans",
			    				
			    				"aggs": {
									    "byProtocolAndPort": {
											      "multi_terms": {
												        "terms": [{"field": "protocol"},
												        		 {"field": "port"}
												        		 ]
											      	}
									    },
									    "aggs" : {
										    "domainsNumber" : { "value_count_distinct" : { "field" : "domains" } }
										  }
									  },
									  
			    				"comparison":{
			    						"sourceField": "domainsNumber",
					      				"operator": ">",
					      				"value": "2"
					    		}
			    				}
			    			],
			    	 "qualifier" : {"aggs": {
									    "groupByIp": {
									      "terms": {
									          "field": "ip.address" 
									        }
									      }
									    }
								  }
	    	}
	}
   }
}


```




### Ingestion of enrichment processors


Enrichment processor files must be placed in a subdirectory of the cyberpedia container:

```
 /home/cyberpedia/data/processors/
```

The ingestion is triggered by the command **cyberpedia ingest**, as for data files.

A glob filter can be used to select the processor

```
cyberpedia ingest --path ./data/processors/ -f *wordpress* 
```

### launching the enrichment process

Similarly to the ingestion process, use the command :

**cyberpedia enrich**

To select the enrichment processor, get the processor id with kibana in the cyberpedia.processors.models index and run

```
cyberpedia enrich -f processor-id
```


### Conventions

 * the json file name must always include the suffix  -enrichments at the end of it's name (e.g favicon(-enrichments.json)
 
 
  
